<?php

global $wpdb;

$infos = array();

if($_POST['action']=='add' && wp_verify_nonce($_POST['ulip_nonce_field'],'ulip-add')){
	$MaestroLink_Fixed_Income_Plus_IDR_BID = $_POST['MaestroLink_Fixed_Income_Plus_IDR_BID'];
	$MaestroLink_Fixed_Income_Plus_IDR_OFFER = $_POST['MaestroLink_Fixed_Income_Plus_IDR_OFFER'];
	$MaestroLink_Cash_Plus_IDR_BID = $_POST['MaestroLink_Cash_Plus_IDR_BID'];
	$MaestroLink_Cash_Plus_IDR_OFFER = $_POST['MaestroLink_Cash_Plus_IDR_OFFER'];
	$MaestroLink_Equity_Plus_IDR_BID = $_POST['MaestroLink_Equity_Plus_IDR_BID'];
	$MaestroLink_Equity_Plus_IDR_OFFER = $_POST['MaestroLink_Equity_Plus_IDR_OFFER'];
	$MaestroLink_Fixed_Income_Plus_USD_BID = $_POST['MaestroLink_Fixed_Income_Plus_USD_BID'];
	$MaestroLink_Fixed_Income_Plus_USD_OFFER = $_POST['MaestroLink_Fixed_Income_Plus_USD_OFFER'];
	$MaestroLink_Balanced_IDR_BID = $_POST['MaestroLink_Balanced_IDR_BID'];
	$MaestroLink_Balanced_IDR_OFFER = $_POST['MaestroLink_Balanced_IDR_OFFER'];
	// $MaestroPiece_Platinum_USD_BID = $_POST['MaestroPiece_Platinum_USD_BID'];
	// $MaestroPiece_Platinum_USD_OFFER = $_POST['MaestroPiece_Platinum_USD_OFFER'];
	// $MaestroPiece_Platinum_02_USD_BID = $_POST['MaestroPiece_Platinum_02_USD_BID'];
	// $MaestroPiece_Platinum_02_USD_OFFER = $_POST['MaestroPiece_Platinum_02_USD_OFFER'];
	$Maestro_Equity_Syariah_IDR_BID = $_POST['Maestro_Equity_Syariah_IDR_BID'];
	$Maestro_Equity_Syariah_IDR_OFFER = $_POST['Maestro_Equity_Syariah_IDR_OFFER'];
	$Maestro_Balance_Syariah_IDR_BID = $_POST['Maestro_Balance_Syariah_IDR_BID'];
	$Maestro_Balance_Syariah_IDR_OFFER = $_POST['Maestro_Balance_Syariah_IDR_OFFER'];
	$MaestroLink_Dynamic_IDR_BID = $_POST['MaestroLink_Dynamic_IDR_BID'];
	$MaestroLink_Dynamic_IDR_OFFER = $_POST['MaestroLink_Dynamic_IDR_OFFER'];
	$MaestroLink_Aggresive_Equity_IDR_BID = $_POST['MaestroLink_Aggresive_Equity_IDR_BID'];
	$MaestroLink_Aggresive_Equity_IDR_OFFER = $_POST['MaestroLink_Aggresive_Equity_IDR_OFFER'];
	$AFI_Dynamic_Money_IDR_BID = $_POST['AFI_Dynamic_Money_IDR_BID'];
	$AFI_Dynamic_Money_IDR_OFFER = $_POST['AFI_Dynamic_Money_IDR_OFFER'];
	$AFI_Progressive_Money_IDR_BID = $_POST['AFI_Progressive_Money_IDR_BID'];
	$AFI_Progressive_Money_IDR_OFFER = $_POST['AFI_Progressive_Money_IDR_OFFER'];
	$AFI_Secure_Money_IDR_BID = $_POST['AFI_Secure_Money_IDR_BID'];
	$AFI_Secure_Money_IDR_OFFER = $_POST['AFI_Secure_Money_IDR_OFFER'];
	$Secure_Money_USD_BID = $_POST['Secure_Money_USD_BID'];
	$Secure_Money_USD_OFFER = $_POST['Secure_Money_USD_OFFER'];
	$Money_Market_IDR_BID = $_POST['Money_Market_IDR_BID'];
	$Money_Market_IDR_OFFER = $_POST['Money_Market_IDR_OFFER'];
	$Syariah_Dynamic_BID = $_POST['Syariah_Dynamic_BID'];
	$Syariah_Dynamic_OFFER = $_POST['Syariah_Dynamic_OFFER'];
	$Syariah_Progressive_BID = $_POST['Syariah_Progressive_BID'];		
	$Syariah_Progressive_OFFER = $_POST['Syariah_Progressive_OFFER'];
	$Maestro_Progressive_Equity_Syariah_BID = $_POST['Maestro_Progressive_Equity_Syariah_BID'];		
	$Maestro_Progressive_Equity_Syariah_OFFER = $_POST['Maestro_Progressive_Equity_Syariah_OFFER'];		
	$Maestrolink_Maxi_Advantage_BID = $_POST['Maestrolink_Maxi_Advantage_BID'];
	$Maestrolink_Maxi_Advantage_OFFER = $_POST['Maestrolink_Maxi_Advantage_OFFER'];
	$Maestrolink_Maxi_Advantage_GP = $_POST['Maestrolink_Maxi_Advantage_GP'];
	$MaestroBerimbang_BID = $_POST['MaestroBerimbang_BID'];
	$MaestroBerimbang_OFFER = $_POST['MaestroBerimbang_OFFER'];		
	$AXA_CitraDinamis_BID = $_POST['AXA_CitraDinamis_BID'];		
	$AXA_CitraDinamis_OFFER = $_POST['AXA_CitraDinamis_OFFER'];		
	$CitraGold_BID = $_POST['CitraGold_BID'];		
	$CitraGold_OFFER = $_POST['CitraGold_OFFER'];		
	$AXA_MaestroDollar_BID = $_POST['AXA_MaestroDollar_BID'];		
	$AXA_MaestroDollar_OFFER = $_POST['AXA_MaestroDollar_OFFER'];		
	$AXA_MaestroObligasi_Plus_BID = $_POST['AXA_MaestroObligasi_Plus_BID'];
	$AXA_MaestroObligasi_Plus_OFFER = $_POST['AXA_MaestroObligasi_Plus_OFFER'];		
	$AXA_MaestroSaham_BID = $_POST['AXA_MaestroSaham_BID'];		
	$AXA_MaestroSaham_OFFER = $_POST['AXA_MaestroSaham_OFFER'];
	$ALI_Dynamic_BID = $_POST['ALI_Dynamic_BID'];
	$ALI_Dynamic_OFFER = $_POST['ALI_Dynamic_OFFER'];
	$ALI_Progressive_BID = $_POST['ALI_Progressive_BID'];
	$ALI_Progressive_OFFER = $_POST['ALI_Progressive_OFFER'];
	$ALI_Secure_BID = $_POST['ALI_Secure_BID'];
	$ALI_Secure_OFFER = $_POST['ALI_Secure_OFFER'];

	//$UlipDate = '`'.$_POST['aa'].'-'.$_POST['mm'].'-'.$_POST['jj'].' '.'00:00:00`';
	$UlipDate = date('Y-m-d H:i:s', mktime(0,0,0, $_POST['mm'],$_POST['jj'],$_POST['aa']));
	
	IF(check_date_duplication()){
	
	$query = $wpdb->query("INSERT INTO wp_ulip_rate(
					Date,
					MaestroLink_Fixed_Income_Plus_IDR_BID,
					MaestroLink_Fixed_Income_Plus_IDR_OFFER,
					MaestroLink_Cash_Plus_IDR_BID,
					MaestroLink_Cash_Plus_IDR_OFFER,
					MaestroLink_Equity_Plus_IDR_BID,
					MaestroLink_Equity_Plus_IDR_OFFER,
					MaestroLink_Fixed_Income_Plus_USD_BID,
					MaestroLink_Fixed_Income_Plus_USD_OFFER,
					MaestroLink_Balanced_IDR_BID,
					MaestroLink_Balanced_IDR_OFFER,
					Maestro_Equity_Syariah_IDR_BID,
					Maestro_Equity_Syariah_IDR_OFFER,
					Maestro_Balance_Syariah_IDR_BID,
					Maestro_Balance_Syariah_IDR_OFFER,
					MaestroLink_Dynamic_IDR_BID,
					MaestroLink_Dynamic_IDR_OFFER,
					MaestroLink_Aggresive_Equity_IDR_BID,
					MaestroLink_Aggresive_Equity_IDR_OFFER,
					AFI_Dynamic_Money_IDR_BID,
					AFI_Dynamic_Money_IDR_OFFER,
					AFI_Progressive_Money_IDR_BID,
					AFI_Progressive_Money_IDR_OFFER,
					AFI_Secure_Money_IDR_BID,
					AFI_Secure_Money_IDR_OFFER,
					Secure_Money_USD_BID,
					Secure_Money_USD_OFFER,
					Money_Market_IDR_BID,
					Money_Market_IDR_OFFER,
					Syariah_Dynamic_BID,
					Syariah_Dynamic_OFFER,
					Syariah_Progressive_BID,
					Syariah_Progressive_OFFER,
					Maestro_Progressive_Equity_Syariah_BID,
					Maestro_Progressive_Equity_Syariah_OFFER,
					Maestrolink_Maxi_Advantage_BID,
					Maestrolink_Maxi_Advantage_OFFER,
					Maestrolink_Maxi_Advantage_GP,
					MaestroBerimbang_BID,
					MaestroBerimbang_OFFER,
					AXA_CitraDinamis_BID,
					AXA_CitraDinamis_OFFER,
					CitraGold_BID,
					CitraGold_OFFER,
					AXA_MaestroDollar_BID,
					AXA_MaestroDollar_OFFER,
					AXA_MaestroObligasi_Plus_BID,
					AXA_MaestroObligasi_Plus_OFFER,
					AXA_MaestroSaham_BID,
					AXA_MaestroSaham_OFFER,
					ALI_Dynamic_BID,
					ALI_Dynamic_OFFER,
					ALI_Progressive_BID,
					ALI_Progressive_OFFER,
					ALI_Secure_BID,
					ALI_Secure_OFFER) 
					
					VALUES(
					
					'$UlipDate',
					$MaestroLink_Fixed_Income_Plus_IDR_BID,
					$MaestroLink_Fixed_Income_Plus_IDR_OFFER,
					$MaestroLink_Cash_Plus_IDR_BID,
					$MaestroLink_Cash_Plus_IDR_OFFER,
					$MaestroLink_Equity_Plus_IDR_BID,
					$MaestroLink_Equity_Plus_IDR_OFFER,
					$MaestroLink_Fixed_Income_Plus_USD_BID,
					$MaestroLink_Fixed_Income_Plus_USD_OFFER,
					$MaestroLink_Balanced_IDR_BID,
					$MaestroLink_Balanced_IDR_OFFER,
					$Maestro_Equity_Syariah_IDR_BID,
					$Maestro_Equity_Syariah_IDR_OFFER,
					$Maestro_Balance_Syariah_IDR_BID,
					$Maestro_Balance_Syariah_IDR_OFFER,
					$MaestroLink_Dynamic_IDR_BID,
					$MaestroLink_Dynamic_IDR_OFFER,
					$MaestroLink_Aggresive_Equity_IDR_BID,
					$MaestroLink_Aggresive_Equity_IDR_OFFER,
					$AFI_Dynamic_Money_IDR_BID,
					$AFI_Dynamic_Money_IDR_OFFER,
					$AFI_Progressive_Money_IDR_BID,
					$AFI_Progressive_Money_IDR_OFFER,
					$AFI_Secure_Money_IDR_BID,
					$AFI_Secure_Money_IDR_OFFER,
					$Secure_Money_USD_BID,
					$Secure_Money_USD_OFFER,
					$Money_Market_IDR_BID,
					$Money_Market_IDR_OFFER,
					$Syariah_Dynamic_BID,
					$Syariah_Dynamic_OFFER,
					$Syariah_Progressive_BID,
					$Syariah_Progressive_OFFER,
					$Maestro_Progressive_Equity_Syariah_BID,
					$Maestro_Progressive_Equity_Syariah_OFFER,
					$Maestrolink_Maxi_Advantage_BID,
					$Maestrolink_Maxi_Advantage_OFFER,
					$Maestrolink_Maxi_Advantage_GP,
					$MaestroBerimbang_BID,
					$MaestroBerimbang_OFFER,
					$AXA_CitraDinamis_BID,
					$AXA_CitraDinamis_OFFER,
					$CitraGold_BID,
					$CitraGold_OFFER,
					$AXA_MaestroDollar_BID,
					$AXA_MaestroDollar_OFFER,
					$AXA_MaestroObligasi_Plus_BID,
					$AXA_MaestroObligasi_Plus_OFFER,
					$AXA_MaestroSaham_BID,
					$AXA_MaestroSaham_OFFER,
					$ALI_Dynamic_BID,
					$ALI_Dynamic_OFFER,
					$ALI_Progressive_BID,
					$ALI_Progressive_OFFER,
					$ALI_Secure_BID,
					$ALI_Secure_OFFER
					)");
	
		if($query) : $infos[] .= $_POST['aa'].'-'.$_POST['mm'].'-'.$_POST['jj'].' ULIP rate is successfully added';
		else : $infos[] .= 'something went wrong, please try again';
		endif;
	} else {
		$infos[] .= 'There\'s already a rate for '.$_POST['aa'].'-'.$_POST['mm'].'-'.$_POST['jj'].', please use edit to change rate';		
	}
}



if($_POST['action']=='edit' && wp_verify_nonce($_POST['ulip_nonce_field'],'ulip-add')){
	$ulipid = $_POST['ulipid'];
	$MaestroLink_Fixed_Income_Plus_IDR_BID = $_POST['MaestroLink_Fixed_Income_Plus_IDR_BID'];
	$MaestroLink_Fixed_Income_Plus_IDR_OFFER = $_POST['MaestroLink_Fixed_Income_Plus_IDR_OFFER'];
	$MaestroLink_Cash_Plus_IDR_BID = $_POST['MaestroLink_Cash_Plus_IDR_BID'];
	$MaestroLink_Cash_Plus_IDR_OFFER = $_POST['MaestroLink_Cash_Plus_IDR_OFFER'];
	$MaestroLink_Equity_Plus_IDR_BID = $_POST['MaestroLink_Equity_Plus_IDR_BID'];
	$MaestroLink_Equity_Plus_IDR_OFFER = $_POST['MaestroLink_Equity_Plus_IDR_OFFER'];
	$MaestroLink_Fixed_Income_Plus_USD_BID = $_POST['MaestroLink_Fixed_Income_Plus_USD_BID'];
	$MaestroLink_Fixed_Income_Plus_USD_OFFER = $_POST['MaestroLink_Fixed_Income_Plus_USD_OFFER'];
	$MaestroLink_Balanced_IDR_BID = $_POST['MaestroLink_Balanced_IDR_BID'];
	$MaestroLink_Balanced_IDR_OFFER = $_POST['MaestroLink_Balanced_IDR_OFFER'];
	$Maestro_Equity_Syariah_IDR_BID = $_POST['Maestro_Equity_Syariah_IDR_BID'];
	$Maestro_Equity_Syariah_IDR_OFFER = $_POST['Maestro_Equity_Syariah_IDR_OFFER'];
	$Maestro_Balance_Syariah_IDR_BID = $_POST['Maestro_Balance_Syariah_IDR_BID'];
	$Maestro_Balance_Syariah_IDR_OFFER = $_POST['Maestro_Balance_Syariah_IDR_OFFER'];
	$MaestroLink_Dynamic_IDR_BID = $_POST['MaestroLink_Dynamic_IDR_BID'];
	$MaestroLink_Dynamic_IDR_OFFER = $_POST['MaestroLink_Dynamic_IDR_OFFER'];
	$MaestroLink_Aggresive_Equity_IDR_BID = $_POST['MaestroLink_Aggresive_Equity_IDR_BID'];
	$MaestroLink_Aggresive_Equity_IDR_OFFER = $_POST['MaestroLink_Aggresive_Equity_IDR_OFFER'];
	$AFI_Dynamic_Money_IDR_BID = $_POST['AFI_Dynamic_Money_IDR_BID'];
	$AFI_Dynamic_Money_IDR_OFFER = $_POST['AFI_Dynamic_Money_IDR_OFFER'];
	$AFI_Progressive_Money_IDR_BID = $_POST['AFI_Progressive_Money_IDR_BID'];
	$AFI_Progressive_Money_IDR_OFFER = $_POST['AFI_Progressive_Money_IDR_OFFER'];
	$AFI_Secure_Money_IDR_BID = $_POST['AFI_Secure_Money_IDR_BID'];
	$AFI_Secure_Money_IDR_OFFER = $_POST['AFI_Secure_Money_IDR_OFFER'];
	$Secure_Money_USD_BID = $_POST['Secure_Money_USD_BID'];
	$Secure_Money_USD_OFFER = $_POST['Secure_Money_USD_OFFER'];
	$Money_Market_IDR_BID = $_POST['Money_Market_IDR_BID'];
	$Money_Market_IDR_OFFER = $_POST['Money_Market_IDR_OFFER'];
	$Syariah_Dynamic_BID = $_POST['Syariah_Dynamic_BID'];
	$Syariah_Dynamic_OFFER = $_POST['Syariah_Dynamic_OFFER'];
	$Syariah_Progressive_BID = $_POST['Syariah_Progressive_BID'];
	$Syariah_Progressive_OFFER = $_POST['Syariah_Progressive_OFFER'];
	$Maestro_Progressive_Equity_Syariah_BID = $_POST['Maestro_Progressive_Equity_Syariah_BID'];
	$Maestro_Progressive_Equity_Syariah_OFFER = $_POST['Maestro_Progressive_Equity_Syariah_OFFER'];
	$Maestrolink_Maxi_Advantage_BID = $_POST['Maestrolink_Maxi_Advantage_BID'];
	$Maestrolink_Maxi_Advantage_OFFER = $_POST['Maestrolink_Maxi_Advantage_OFFER'];
	$Maestrolink_Maxi_Advantage_GP = $_POST['Maestrolink_Maxi_Advantage_GP'];
	$MaestroBerimbang_BID = $_POST['MaestroBerimbang_BID'];
	$MaestroBerimbang_OFFER = $_POST['MaestroBerimbang_OFFER'];
	$AXA_CitraDinamis_BID = $_POST['AXA_CitraDinamis_BID'];
	$AXA_CitraDinamis_OFFER = $_POST['AXA_CitraDinamis_OFFER'];
	$CitraGold_BID = $_POST['CitraGold_BID'];
	$CitraGold_OFFER = $_POST['CitraGold_OFFER'];
	$AXA_MaestroDollar_BID = $_POST['AXA_MaestroDollar_BID'];
	$AXA_MaestroDollar_OFFER = $_POST['AXA_MaestroDollar_OFFER'];
	$AXA_MaestroObligasi_Plus_BID = $_POST['AXA_MaestroObligasi_Plus_BID'];
	$AXA_MaestroObligasi_Plus_OFFER = $_POST['AXA_MaestroObligasi_Plus_OFFER'];
	$AXA_MaestroSaham_BID = $_POST['AXA_MaestroSaham_BID'];
	$AXA_MaestroSaham_OFFER = $_POST['AXA_MaestroSaham_OFFER'];
	$ALI_Dynamic_BID = $_POST['ALI_Dynamic_BID'];
	$ALI_Dynamic_OFFER = $_POST['ALI_Dynamic_OFFER'];
	$ALI_Progressive_BID = $_POST['ALI_Progressive_BID'];
	$ALI_Progressive_OFFER = $_POST['ALI_Progressive_OFFER'];
	$ALI_Secure_BID = $_POST['ALI_Secure_BID'];
	$ALI_Secure_OFFER = $_POST['ALI_Secure_OFFER'];


	$query = $wpdb->update('wp_ulip_rate',array(
					'MaestroLink_Fixed_Income_Plus_IDR_BID'=>$MaestroLink_Fixed_Income_Plus_IDR_BID,
					'MaestroLink_Fixed_Income_Plus_IDR_OFFER'=>$MaestroLink_Fixed_Income_Plus_IDR_OFFER,
					'MaestroLink_Cash_Plus_IDR_BID'=>$MaestroLink_Cash_Plus_IDR_BID,
					'MaestroLink_Cash_Plus_IDR_OFFER'=>$MaestroLink_Cash_Plus_IDR_OFFER,
					'MaestroLink_Equity_Plus_IDR_BID'=>$MaestroLink_Equity_Plus_IDR_BID,
					'MaestroLink_Equity_Plus_IDR_OFFER'=>$MaestroLink_Equity_Plus_IDR_OFFER,
					'MaestroLink_Fixed_Income_Plus_USD_BID'=>$MaestroLink_Fixed_Income_Plus_USD_BID,
					'MaestroLink_Fixed_Income_Plus_USD_OFFER'=>$MaestroLink_Fixed_Income_Plus_USD_OFFER,
					'MaestroLink_Balanced_IDR_BID'=>$MaestroLink_Balanced_IDR_BID,
					'MaestroLink_Balanced_IDR_OFFER'=>$MaestroLink_Balanced_IDR_OFFER,
					'Maestro_Equity_Syariah_IDR_BID'=>$Maestro_Equity_Syariah_IDR_BID,
					'Maestro_Equity_Syariah_IDR_OFFER'=>$Maestro_Equity_Syariah_IDR_OFFER,
					'Maestro_Balance_Syariah_IDR_BID'=>$Maestro_Balance_Syariah_IDR_BID,
					'Maestro_Balance_Syariah_IDR_OFFER'=>$Maestro_Balance_Syariah_IDR_OFFER,
					'MaestroLink_Dynamic_IDR_BID'=>$MaestroLink_Dynamic_IDR_BID,
					'MaestroLink_Dynamic_IDR_OFFER'=>$MaestroLink_Dynamic_IDR_OFFER,
					'MaestroLink_Aggresive_Equity_IDR_BID'=>$MaestroLink_Aggresive_Equity_IDR_BID,
					'MaestroLink_Aggresive_Equity_IDR_OFFER'=>$MaestroLink_Aggresive_Equity_IDR_OFFER,
					'AFI_Dynamic_Money_IDR_BID'=>$AFI_Dynamic_Money_IDR_BID,
					'AFI_Dynamic_Money_IDR_OFFER'=>$AFI_Dynamic_Money_IDR_OFFER,
					'AFI_Progressive_Money_IDR_BID'=>$AFI_Progressive_Money_IDR_BID,
					'AFI_Progressive_Money_IDR_OFFER'=>$AFI_Progressive_Money_IDR_OFFER,
					'AFI_Secure_Money_IDR_BID'=>$AFI_Secure_Money_IDR_BID,
					'AFI_Secure_Money_IDR_OFFER'=>$AFI_Secure_Money_IDR_OFFER,
					'Secure_Money_USD_BID'=>$Secure_Money_USD_BID,
					'Secure_Money_USD_OFFER'=>$Secure_Money_USD_OFFER,
					'Money_Market_IDR_BID'=>$Money_Market_IDR_BID,
					'Money_Market_IDR_OFFER'=>$Money_Market_IDR_OFFER,
					'Syariah_Dynamic_BID'=>$Syariah_Dynamic_BID,
					'Syariah_Dynamic_OFFER'=>$Syariah_Dynamic_OFFER,
					'Syariah_Progressive_BID'=>$Syariah_Progressive_BID,	
					'Syariah_Progressive_OFFER'=>$Syariah_Progressive_OFFER,
					'Maestro_Progressive_Equity_Syariah_BID'=>$Maestro_Progressive_Equity_Syariah_BID,
					'Maestro_Progressive_Equity_Syariah_OFFER'=>$Maestro_Progressive_Equity_Syariah_OFFER,
					'Maestrolink_Maxi_Advantage_BID'=>$Maestrolink_Maxi_Advantage_BID,
					'Maestrolink_Maxi_Advantage_OFFER'=>$Maestrolink_Maxi_Advantage_OFFER,
					'Maestrolink_Maxi_Advantage_GP'=>$Maestrolink_Maxi_Advantage_GP,
					'MaestroBerimbang_BID'=>$MaestroBerimbang_BID,
					'MaestroBerimbang_OFFER'=>$MaestroBerimbang_OFFER,
					'AXA_CitraDinamis_BID'=>$AXA_CitraDinamis_BID,
					'AXA_CitraDinamis_OFFER'=>$AXA_CitraDinamis_OFFER,
					'CitraGold_BID'=>$CitraGold_BID,
					'CitraGold_OFFER'=>$CitraGold_OFFER,
					'AXA_MaestroDollar_BID'=>$AXA_MaestroDollar_BID,
					'AXA_MaestroDollar_OFFER'=>$AXA_MaestroDollar_OFFER,
					'AXA_MaestroObligasi_Plus_BID'=>$AXA_MaestroObligasi_Plus_BID,
					'AXA_MaestroObligasi_Plus_OFFER'=>$AXA_MaestroObligasi_Plus_OFFER,
					'AXA_MaestroSaham_BID'=>$AXA_MaestroSaham_BID,
					'AXA_MaestroSaham_OFFER'=>$AXA_MaestroSaham_OFFER,
					'ALI_Dynamic_BID'=>$ALI_Dynamic_BID,
					'ALI_Dynamic_OFFER'=>$ALI_Dynamic_OFFER,
					'ALI_Progressive_BID'=>$ALI_Progressive_BID,
					'ALI_Progressive_OFFER'=>$ALI_Progressive_OFFER,
					'ALI_Secure_BID'=>$ALI_Secure_BID,
					'ALI_Secure_OFFER'=>$ALI_Secure_OFFER
					),array('id' => $ulipid)
			);

	if( $query >= 0 ) : $infos[] .= 'ULIP rate is successfully modified';
	else : $infos[] .= 'something went wrong, please try again';
	endif;
}

function check_date_duplication(){
	global $wpdb;		
	$today = $_POST['aa'].'-'.$_POST['mm'].'-'.$_POST['jj'];
	$rowdate = $wpdb->get_var("SELECT * FROM wp_ulip_rate WHERE Date LIKE '%$today%'");

	if(empty($rowdate)){
		return true;
	} else {
		return false;
	}
}

?>