<?php 
class AXA_Ulip{
	
	protected $ulip = "all";
	protected $product;
	
	public function get_data($data,$product){
		global $wpdb;
	
		if( $data = 'all' && empty($product)) :
			$query = $wpdb->get_results("SELECT * FROM wp_ulip_rate ORDER BY Date DESC LIMIT 2",ARRAY_A);
		elseif( $data = 'product' && !empty($product) && !isset($_GET['datequery']) ) :
			$unit = $product;
			$product = $product.'_BID,'.$product.'_OFFER';
			$query = $wpdb->get_results("SELECT Date,$product FROM wp_ulip_rate where ".$unit."_BID != 0.0000 ORDER BY Date DESC LIMIT 10");
		elseif( $data = 'product' && !empty($product) && isset($_GET['datequery']) && !empty($_GET['datequery']) ) :
			$unit = $product;
			$product = $product.'_BID,'.$product.'_OFFER';
			$startDate = $_GET['startDate'];
			$endDate = $_GET['endDate'];
			$endDateTime = strtotime($endDate . ' + 1 day');
			$endDateString = date('Y-m-d',strtotime($endDate . ' + 1 day'));
			$query = $wpdb->get_results("SELECT Date, $product FROM wp_ulip_rate WHERE ".$unit."_BID != 0.0000 AND Date BETWEEN '$startDate' AND '$endDateString' ORDER BY Date DESC");
		endif;
		
		return $query;
	}

	public function get_data_guaranteed_price(){
		global $wpdb;
	
		$query = $wpdb->get_results("SELECT * FROM wp_ulip_rate where Maestrolink_Maxi_Advantage_GP != '0.0000' ORDER BY Date DESC LIMIT 1",ARRAY_A);
		
		return $query;
	}
	
	function construct_data_all($query){
			
		//Set Product Title
		$newstructure[0][] = __('Jenis Produk','axa-financial');
		$newstructure[1][] = 'Active Money IDR  ';
		$newstructure[2][] = 'Active Money Syariah IDR  ';
		$newstructure[3][] = 'Advanced Commodity IDR  ';
		$newstructure[4][] = 'Amanah Equity Syariah IDR  ';	
        $newstructure[5][] = 'Attractive Money IDR  ';
        $newstructure[6][] = 'Attractive Money Syariah IDR  ';
        $newstructure[7][] = 'Dynamic Money IDR  ';
        $newstructure[8][] = 'Excellent Equity IDR  ';
        $newstructure[9][] = 'Fixed Money IDR  ';
        $newstructure[10][] = 'Maestrolink Maxi Advantage  ';
        $newstructure[11][] = 'Money Market IDR  '; 
        $newstructure[12][] = 'Progressive Money IDR  ';
        $newstructure[13][] = 'Protected Money IDR  ';
        $newstructure[14][] = 'Money Market IDR  ';  
        $newstructure[15][] = 'Secure Money IDR  ';
        $newstructure[16][] = 'Secure Money USD  ';

		foreach ($query as $key=>$value) {
			
			
			$newstructure[0][] = $value['Date'];
			$newstructure[1][] = $value['Active_Money_IDR_BID'];
			$newstructure[2][] = $value['Active_Money_Syariah_IDR_BID'];
			$newstructure[3][] = $value['Advanced_Commodity_IDR_BID'];
			$newstructure[4][] = $value['Amanah_Equity_Syariah_IDR_BID'];	
	        $newstructure[5][] = $value['Attractive_Money_IDR_BID'];
	        $newstructure[6][] = $value['Attractive_Money_Syariah_IDR_BID'];
	        $newstructure[7][] = $value['Dynamic_Money_IDR_BID'];
	        $newstructure[8][] = $value['Excellent_Equity_IDR_BID'];
	        $newstructure[9][] = $value['Fixed_Money_IDR_BID'];
	        $newstructure[10][] = $value['Maestrolink_Maxi_Advantage_BID'];
	        $newstructure[11][] = $value['Money_Market_IDR_BID']; 
	        $newstructure[12][] = $value['Progressive_Money_IDR_BID'];
	        $newstructure[13][] = $value['Protected_Money_IDR_BID'];
	        $newstructure[14][] = $value['Money_Market_IDR_BID'];  
	        $newstructure[15][] = $value['Secure_Money_IDR_BID'];
	        $newstructure[16][] = $value['Secure_Money_USD_BID'];
	 
			
			}
		$count= count($newstructure);
		for($i=1;$i<$count;$i++){
			$newstructure[$i][] = $newstructure[$i][1]-$newstructure[$i][2];	
		}

		$newstructure[1][] = 'active-money-idr';
		$newstructure[2][] = 'active-money-syariah-idr';
		$newstructure[3][] = 'advanced-commodity-idr';
		$newstructure[4][] = 'amanah-equity-syariah-idr';
        $newstructure[5][] = 'attractive-money-idr';        
        $newstructure[6][] = 'attractive-money-syariah-idr';
        $newstructure[7][] = 'dynamic-money-idr';
        $newstructure[8][] = 'excellent-equity-idr';
        $newstructure[9][] = 'fixed-money-idr';
        $newstructure[10][] = 'maestro-link-maxi-advantage-idr';
        $newstructure[11][] = 'money-market-idr'; 
        $newstructure[12][] = 'progressive-money-idr';
        $newstructure[13][] = 'protected-money-idr';
        $newstructure[14][] = 'money-market-idr';  
        $newstructure[15][] = 'secure-money-idr';
        $newstructure[16][] = 'secure-money-usd';
    

		return $newstructure;
	
	}

	function construct_data_product($query,$product,$hide_offer){
			
		//setlocale(LC_ALL, 'id_ID', 'IND', 'Indonesian', 'Indonesia');
		//Note: Offer is hidden pending further notice remove HTML comment <!-- --> when necessary
		if($query) :
			$productBid = $product.'_BID';
			$productOffer = $product.'_OFFER';
			$output = "<table width='420px' id='unit-table' cellspacing='0'>";
			if($hide_offer == 1) : 
				$output .= "<thead>
					<tr>
						<th>".__('Tanggal','axa-financial')."</th>
						<th>".__('Harga Bid','axa-financial')."</th>
					</tr>
					</thead>
					<tbody>";

						foreach($query as $value){
							$output .= "<tr><td>".strftime('%e/%m/%Y',strtotime($value->Date))."</td>";	
							$output .= "<td>".number_format($value->$productBid,'2','.',',')."</td></tr>";	
						}
				$output .= "</tbody></table>";
			else :
				$output .= "<thead>
					<tr>
						<th>".__('Tanggal','axa-financial')."</th>
						<th>".__('Harga Bid','axa-financial')."</th>
						<th>".__('Harga Offer','axa-financial')."</th>
					</tr>
					</thead>
					<tbody>";

						foreach($query as $value){
							$output .= "<tr><td>".strftime('%e/%m/%Y',strtotime($value->Date))."</td>";	
							$output .= "<td>".number_format($value->$productBid,'2','.',',')."</td>";	
							$output .= "<td>".number_format($value->$productOffer,'2','.',',')."</td></tr>";	
						}
				$output .= "</tbody></table>";
			endif;
			
			//$output .= "<div class='news-pagination'>".wp_pagenavi(array( 'query' => $query ))."</div>";
		else :
			$output = __('Data tidak tersedia untuk tanggal ini','axa-financial');
		endif;
		echo $output;
	}

	function set_table($add){
		
		setlocale(LC_ALL, 'id_ID', 'IND', 'Indonesian', 'Indonesia');
		$newstructure = $add;
			
		$output  = "<table id='unit-table' cellspacing='0' width='100%'>";
		$output .= 	"<thead>
						<tr>
							<th>".$newstructure[0][0]."</th>
							<th>".strftime( '%e/%m/%Y',strtotime($newstructure[0][1]) )."</th>
							<th>".strftime( '%e/%m/%Y',strtotime($newstructure[0][2]) )."</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>";
		unset($newstructure[0]);
		
		sort($newstructure);
		
		foreach($newstructure as $value){
			$output .= "<tr>";
			$output .= "<td class=\"product-name\">".$value[0]."</td>"."<td>".number_format($value[1],'4','.',',')."</td>"."<td>".number_format($value[2],'4','.',',')."</td>";
			
			if ( $value[3] < 0 ){ $class = "min"; } else { $class = "up"; }
			
			$output .= "<td class=\"allresult ".$class."\"><span></span>".number_format(abs($value[3]),4)."</td>"."<td><a href=\"".$value[4]."\" class=\"btn-medium\">".__('selengkapnya','axa-financial')."</a></td>";
			$output .= "</tr>";
		}
		
		$output .= "</tbody></table>";
		
		return $output;
	}
		
	function set_widget($add){
		//setlocale(LC_ALL, 'id_ID', 'IND', 'Indonesian', 'Indonesia');
		$newstructure = $add;

		$older_date = strftime('%e/%m/%Y',strtotime($newstructure[0][1]));
		$latest_date = strftime('%e/%m/%Y',strtotime($newstructure[0][2]));
		
		$output = "<div class='slideshow-unit owl-carousel'>";
		unset($newstructure[0]);
		sort($newstructure);
		
		foreach($newstructure as $value){
			if($value[1]!="0.0000"||$value[2]!="0.0000"){
		$output .= "<div class='slide-unit'>";
		$output .= "<ul class='unit-item'>";		
		$output .= "<li><h4 class='product-title'>".$value[0]."</h4></li>";
		$output .= "<li><div class='unit'>";
		$output .= "<span class='date'>".$older_date."</span>";
		$output .= "<span class='result'>".number_format($value[1],'4','.',',')."</span>";
		$output .= "</div></li>";
		$output .= "<li><div class='unit'>";
		$output .= "<span class='date'>".$latest_date."</span>";
		$output .= "<span class='result'>".number_format($value[2],'4','.',',')."</span>";
		$output .= "</div></li>";			
		
		if ( $value[3] < 0 ){ $class = "min"; } else { $class = "up"; }
		$output .= "<li><div class='allresult ".$class."'>".number_format(abs($value[3]),'4','.',',')."</div></li>";				
		$output .= "</ul>";		
		$output .= "</div>";				
		}

		}
		$output .= "</div>";
		return $output;		
	}

	function set_sidebar($add)
	{
		//setlocale(LC_ALL, 'id_ID', 'IND', 'Indonesian', 'Indonesia');
		$newstructure = $add;

		$date = strftime('%e/%m/%Y',strtotime($newstructure[0]['Date']));
		

		$output = 	'<div id="guaranteed_price" class="bottom-widget clearfix last-child" style="display:none">
	                    <div class="left section">
	                        <h5>Harga Unit Dijamin</h5>
	                        <p class="date">'.$date.'</p>
	                    </div>
	                    
	                    <div class="right section" style="width:175px">
	                        <table cellspacing="0;">
	                            <tbody><tr>
	                                <td><small>IDR</small></td>
	                                <td><span style="text-align:right">'.$newstructure[0]['Maestrolink_Maxi_Advantage_GP'].'</span></td>
	                            	
	                            </tr>
	                            <tr>
	                            <td></td>
	                            </tr>
	                        </tbody></table>
	                        <div><span style="text-align:left; font-size:10px; display:block; margin-left: 10px;">Harga unit dijamin dapat naik atau stabil, tapi tidak akan turun</span></div>
	                    </div>
	                </div>';

		return $output;
	}
}

//Display Ulip
function display_ulip($atts){
	$datequery = $_GET['datequery'];
	$hide_offer = $_GET['via_ajax']; 
	//extract ulip shortcode attribute	
	extract(shortcode_atts(array(
		'ulip' => 'all',
		'product' => '',
		),$atts));
		
	$Ulip = new AXA_Ulip;

	$getdata = $Ulip->get_data($ulip,$product);	

	/* Editan Said
	===============================================*/
	if( $ulip = 'all' && empty($product) ) : 
			$add = $Ulip->construct_data_all($getdata);
			return $Ulip->set_table($add);
	elseif( $ulip = 'product' && !empty($product) && !isset($datequery)) : $add = $Ulip->construct_data_product($getdata,$product,$hide_offer);
	elseif( $ulip = 'product' && !empty($product) && isset($datequery) ) : $add = $Ulip->construct_data_product($getdata,$product,$hide_offer);

	endif;
	//echo json_encode($getdata);
}

function display_ulip_widget(){
	$Ulip = new AXA_Ulip;
	$getdata = $Ulip->get_data('all','');
	$add = $Ulip->construct_data_all($getdata);
	return $Ulip->set_widget($add);
}

function display_ulip_gp(){
	$Ulip = new AXA_Ulip;

	$getdata = $Ulip->get_data_guaranteed_price();	

	/* Editan Said
	===============================================*/
	//$add = $Ulip->construct_data_all($getdata);
	return $Ulip->set_sidebar($Ulip->get_data_guaranteed_price());
	//echo json_encode($getdata);
}

add_shortcode('hargaunitharian','display_ulip');
add_shortcode('widgetunitharian','display_ulip_widget');
add_shortcode('sidebar_guaranteed_price','display_ulip_gp');
?>