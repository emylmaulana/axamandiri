jQuery(document).ready(function($){
	//form validation
	$('#ulip-edit').validate({
		  rules: {
		    MaestroLink_Fixed_Income_Plus_IDR : {
		      required: true,
		      number: true
		    },
		    MaestroLink_Cash_Plus_IDR : {
		      required: true,
		      number: true
		    },
		    MaestroLink_Equity_Plus_IDR : {
		      required: true,
		      number: true
		    },
		   	MaestroLink_Fixed_Income_Plus_USD : {
		      required: true,
		      number: true
		    },
		    MaestroLink_Balanced_IDR : {
		      required: true,
		      number: true
		    },
		   
		    Maestro_Equity_Syariah_IDR : {
		      required: true,
		      number: true
		    },
		    Maestro_Balance_Syariah_IDR : {
		      required: true,
		      number: true
		    },
		    MaestroLink_Dynamic_IDR : {
		      required: true,
		      number: true
		    },
		    MaestroLink_Aggresive_Equity_IDR : {
		      required: true,
		      number: true
		    },
		    AFI_Dynamic_Money_IDR : {
		      required: true,
		      number: true
		    },
		    AFI_Progressive_Money_IDR : {
		      required: true,
		      number: true
		    },
		    AFI_Secure_Money_IDR : {
		      required: true,
		      number: true
		    },
		    Secure_Money_USD : {
		      required: true,
		      number: true
		    },
		    Money_Market_IDR : {
		      required: true,
		      number: true
		    },
		    Syariah_Dynamic : {
		      required: true,
		      number: true
		    },
		    Syariah_Progressive : {
		      required: true,
		      number: true
		    },
		    Maestro_Progressive_Equity_Syariah : {
		      required: true,
		      number: true
		    },
		    MaestroBerimbang : {
		      required: true,
		      number: true
		    },
		    AXA_CitraDinamis : {
		      required: true,
		      number: true
		    },
		    CitraGold : {
		      required: true,
		      number: true
		    },
		    AXA_MaestroDollar : {
		      required: true,
		      number: true
		    },
		    AXA_MaestroObligasi_Plus : {
		      required: true,
		      number: true
		    },
		    AXA_MaestroSaham : {
		      required: true,
		      number: true
		    },
		    ALI_Dynamic : {
		      required: true,
		      number: true
		    },
		    ALI_Progressive : {
		      required: true,
		      number: true
		    },
		    ALI_Secure : {
		      required: true,
		      number: true
		    },		  		    
		  }		
	});
})
