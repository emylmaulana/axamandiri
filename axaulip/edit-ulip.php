<?php
// kill the page when someone have unsufficient privilege
if( !current_user_can('manage_options') ) wp_die(__('You do not have sufficient permissions to import content in this site.'));

require_once('ulip.php');

$title = __('ULIP Rate');


if(!isset($_GET['action'])){
	$action = "add";
} else {
	$action = $_GET['action'];
}


if( $_GET['action']=="edit" && isset($_GET['id'])){
	
	global $wpdb;
	$ulipid = $_GET['id']; 
	$query = $wpdb->get_row("SELECT * FROM wp_ulip_rate WHERE id=$ulipid", ARRAY_A);

	$timestamp = strtotime($query['Date']);	
	$day = date('d', $timestamp);
	$month = date('m', $timestamp);
	$year = date('Y', $timestamp);
}

?>


<div class="wrap">
	<h2><?php echo $title; ?></h2>
	<p><?php _e('This page is for adding ULIP rate'); ?></p>
	

	<?php 
		if(count($infos) > 0 ){
	?>
	<div class="infos">
	<?php			
			foreach ($infos as $value) {
				echo $value;
			}
	?>
	</div>
	<?php		
		}
	?>
	
	<form id="ulip-edit" class="form"name="ulip-edit" method="post" action="">
	<input type="hidden" name="action" value="<?php echo $action ?>"/>
	<input type="hidden" name="ulipid" value="<?php echo $ulipid ?>"/>
	<?php wp_nonce_field('ulip-add','ulip_nonce_field'); ?>
		<table width="100%" >
			<tr>
				<td><label for="Date">Date</label></td>
				<td>
					
					<div class="timestamp-wrap">
						<input type="text" id="jj" name="jj" value="<?php echo ($day) ? $day : date("d", time() - 60 * 60 * 24); ?>" size="2" maxlength="2" tabindex="4" autocomplete="off" style="text-align: center;">&nbsp;&nbsp;/&nbsp;
						<select id="mm" name="mm" tabindex="4">
							<option value="01">Jan</option>
							<option value="02">Feb</option>
							<option value="03">Mar</option>
							<option value="04">Apr</option>
							<option value="05">May</option>
							<option value="06">Jun</option>
							<option value="07">Jul</option>
							<option value="08">Aug</option>
							<option value="09">Sep</option>
							<option value="10">Oct</option>
							<option value="11">Nov</option>
							<option value="12">Dec</option>
						</select>
						
						&nbsp;/&nbsp;&nbsp; <input type="text" id="aa" name="aa" value="<?php echo ($year) ? $year : date("Y"); ?>" size="4" maxlength="4" tabindex="4" autocomplete="off"  style="text-align: center;">
					</div>					

				</td>

			</tr>
			<?php 
			$columnsGP=$wpdb->get_results("show columns from wp_ulip_rate where Field!='Date' and Field!='id' AND Field like '%GP%' ");
			foreach ($columnsGP as $columnGP ):
				$gp= str_replace('_GP','',$columnGP->Field);
				$columns = $wpdb->get_results("show columns from wp_ulip_rate where Field!='Date' and Field!='id' AND Field NOT like '%".$gp."%' and Field like '%BID%' ");
				foreach($columns as $column):
					//echo $column->Field.",";
					$r=array("_BID");
					$product=str_replace($r, '', $column->Field);
					
			?>
			<tr>
				<?php $rpl = array("_BID","_");?>
				<td><label for="<?=str_replace($rpl, ' ', $column->Field)?>"><?=str_replace($rpl, ' ', $column->Field)?></label></td>
				<td>Bid: <input type="text" name="<?=$column->Field?>" id="<?=$column->Field?>" value="<?php echo $query[$column->Field]; ?>" /></td>
				<td>Offer: <input type="text" name="<?=str_replace('BID', 'OFFER', $column->Field)?>" id="<?=str_replace('BID', 'OFFER', $column->Field)?>" value="<?php echo $query[str_replace('BID', 'OFFER', $column->Field)]; ?>" /></td>
			
			<?php 
				endforeach;
			endforeach;
			 ?>
			</tr>
			<?php 
				$columns = $wpdb->get_results("show columns from wp_ulip_rate where Field!='Date' and Field!='id' and Field like '%GP%'");
				foreach ($columns as $column):
				
			?>
			<tr>	
				<?php $rpl = array("_GP","_");?>
				<td><label for="<?=str_replace($rpl, ' ', $column->Field)?>"><?=str_replace($rpl, ' ', $column->Field)?></label></td>
				<td>Bid: <input type="text" name="<?=str_replace('GP', 'BID', $column->Field)?>" id="<?=str_replace('GP', 'BID', $column->Field)?>" value="<?php echo $query[$column->Field]?>" /></td>
				<td>Offer: <input type="text" name="<?=str_replace('GP', 'OFFER', $column->Field)?>" id="<?=str_replace('GP', 'OFFER', $column->Field)?>" value="<?php echo $query[str_replace('GP', 'OFFER', $column->Field)]; ?>" /></td>
				<td>GP: <input type="text" name="<?=$column->Field?>" id="<?=$column->Field?>" value="<?php echo $query[$column->Field]; ?>" /></td>
			
			<?php endforeach; ?>

			</tr>
			
			

			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" name="submit" value="Submit" /></td>
			</tr>
			
		</table>
	</form>
	

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" >

		var desiredValue = <?php echo ($month) ? $month : date('m'); ?>;
		var el = document.getElementById("mm");
		for(var i=0; i<el.options.length; i++) {
		  if ( el.options[i].value == desiredValue ) {
		    el.selectedIndex = i;

		    break;
		  }
		}

		jQuery('input[type=text]').change(function() {
		    jQuery(this).val(jQuery(this).val().replace(/,/g,''));
		});		

	</script>
	
</div>
