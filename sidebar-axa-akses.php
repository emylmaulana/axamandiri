<div id="layanan" class="grey-2 h-350 clearfix">
	<div class="content-widget akses">
		<div class="icon"></div>
			<div class="details">
				<h4><?php _e("<!--:en-->AXA AKSES<!--:--><!--:id-->AXA AKSES<!--:-->"); ?></h4>
				<p class="show-for-large-only"><?php _e("<!--:en-->Take advantage of the services AXA SMS and AXA Voice which operates 24 hours, AXA Call and Customer Service in every day and working hours<!--:--><!--:id-->Manfaatkan layanan AXA SMS dan AXA Voice yang beroperasi 24 jam, AXA Call, serta Customer Service di setiap hari dan jam kerja<!--:-->"); ?></p>
					<a href="<?php echo site_url('layanan-nasabah/axa-akses/');?>" class="button blue m-top-45">Hubungi</a>
		</div>
	</div>
</div>