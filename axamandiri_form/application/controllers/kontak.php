<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kontak extends Ci_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('kontak_model');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('email');
		$this->load->library('excel');
	}

	function index(){		
		
	}

	function submit_data(){
	date_default_timezone_set('Asia/Bangkok');
	$product=$this->input->post('product_matrix', TRUE);
		$data = array('nama_lengkap'=> $this->input->post('nama_lengkap', TRUE),
					'no_tlp' => $this->input->post('no_tlp'),					
					'email' => $this->input->post('email', TRUE),
					'tgl_lahir' => $this->input->post('tgl_lahir', TRUE),
					'propinsi' => $this->input->post('propinsi', TRUE),
					'kota' => $this->input->post('kota', TRUE),
					'submit_time' => date('Y-m-d/H:i:s'),
					'product_matrix' => $this->input->post('product_matrix', TRUE),
					'nama_produk' => $this->input->post('nama_produk', TRUE),
					'banner_source' => $this->input->post('banner_source', TRUE),
					'utm_source' => $this->input->post('utm_source', TRUE),
					'utm_medium' => $this->input->post('utm_medium', TRUE),
					'utm_term' => $this->input->post('utm_term', TRUE),
					'utm_content' => $this->input->post('utm_content', TRUE),
					'utm_campaign' => $this->input->post('utm_campaign', TRUE),
					'gclid' => $this->input->post('gclid', TRUE),							
					);
				
		$this->kontak_model->insertData('kontak',$data);
		
		/*EMAIL FEEDBACK TO REGISTRAN*/
		$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE));
		
		$sql = "SELECT * from entity_customer_care where nama_entity='".$this->input->post('product_matrix')."'";
		$query = $this->db->query($sql);
		$results = $query->result_array();//$result = $query->result_array();
		foreach ($results as $rows) {
				$this->email->from($rows['email_from'],'AXA Mandiri');
				// $this->email->from('emilddurkheim@gmail.com');
		}		
		// var_dump($sql);
		// $this->email->from($query['email_from'],'AXA Mandiri');
		// $this->email->to('emilddurkheim@gmail.com');
		$this->email->to($this->input->post('email'));
		$this->email->cc('digital@axa.co.id');
		$this->email->bcc('axa.report.dump@gmail.com'); 
		$this->email->subject("AXA Mandiri | Thank You");
		$this->email->message(
			'<p>Dear '.$this->input->post('nama_lengkap',TRUE).
			'</p>'. 
			'<p>Terima Kasih atas ketertarikan anda terhadap solusi AXA Mandiri. </p> </br>'. 
			
			'<p>Kami akan menghubungi anda</p>'.
			  
			'<p>Terima kasih</p>'.
			'<p>Salam AXA Mandiri</p>'
		);
		if($this->email->send()) {
			$slug=$_GET['slug'];
		
		?>
			<script>window.location = "<?php echo base_url(); ?>/product/<?php echo $slug;?>?status=Data+Terkirim"; </script>
		<?php

		}else{
			echo "Data tidak terkirim";
		}
	

		

	}

	function daily(){
		date_default_timezone_set('Asia/Bangkok');

		$sql = "select * from entity_customer_care order by nama_entity asc";
		
		$query = $this->db->query($sql);
		$result = $query->result_array();

		foreach($result as $row)
		{
			$sql2 = "SELECT * from kontak where product_matrix = '".$row['nama_entity']."' and daily = 0 order by product_matrix asc";
			
			$query2 = $this->db->query($sql2);
			$result2 = $query2->result_array();
			$count = 2;
			
			if ($query2->num_rows() > 0)
			{	
				$this->email->clear(TRUE);
				$sheet = new PHPExcel();
				$sheet->setActiveSheetIndex(0);
				//name the worksheet
				$sheet->getActiveSheet()->setTitle($row['nama_entity']);
				$sheet->getActiveSheet()
								->setCellValue('A1', 'No')
								->setCellValue('B1', 'Nama Lengkap')
								->setCellValue('C1', 'No. Telepon')
								->setCellValue('D1', 'Email')
								->setCellValue('E1', 'Tgl. Lahir')
								->setCellValue('F1', 'Propinsi')
								->setCellValue('G1', 'Kota')
								->setCellValue('H1', 'Product Matrix')
								->setCellValue('I1', 'Nama Produk')
								->setCellValue('J1', 'Waktu Submit');
							
				$sheet->getActiveSheet()->getColumnDimension('A')->setWidth(12);
				$sheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('D')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('E')->setWidth(32);
				$sheet->getActiveSheet()->getColumnDimension('F')->setWidth(32);
				$sheet->getActiveSheet()->getColumnDimension('G')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('H')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('I')->setWidth(20);
				$sheet->getActiveSheet()->getColumnDimension('J')->setWidth(20);

				$sheet->getActiveSheet()->getStyle('A')->getNumberFormat()->setFormatCode('0');
				
				
				$sheet->getActiveSheet()->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$sheet->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


				foreach($result2 as $row2)
				{
					$sheet->setActiveSheetIndex()->setCellValue("A".$count, $count-1);
					$sheet->setActiveSheetIndex()->setCellValue("B".$count, $row2['nama_lengkap']);
					$sheet->getActiveSheet()->setCellValueExplicit("C".$count,$row2['no_tlp'],PHPExcel_Cell_DataType::TYPE_STRING);
					$sheet->setActiveSheetIndex()->setCellValue("D".$count, $row2['email']);
					$sheet->setActiveSheetIndex()->setCellValue("E".$count, $row2['tgl_lahir']);
					$sheet->setActiveSheetIndex()->setCellValue("F".$count, $row2['propinsi']);
					$sheet->setActiveSheetIndex()->setCellValue("G".$count, $row2['kota']);
					$sheet->setActiveSheetIndex()->setCellValue("H".$count, $row2['product_matrix']);
					$sheet->setActiveSheetIndex()->setCellValue("I".$count, $row2['nama_produk']);
					$sheet->setActiveSheetIndex()->setCellValue("J".$count, $row2['submit_time']);
					$count++;
				}

				$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');  
				// header('Content-type: application/vnd.ms-excel');

				// // It will be called file.xls
				// header('Content-Disposition: attachment; filename="file.xls"');

				// // Write file to the browser
				// $objWriter->save('php://output');
				
				$objWriter->save('/var/www/html/axamandiri/axamandiri_form/axamandiri_temp/DailyReport-'.$row['nama_entity'].'-'.date('d-m-Y').'.xls');
				$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE));
				$this->email->from($row['email_from'],'AXA Mandiri');
				$this->email->to($row['email_to']);
				// $this->email->to('emilddurkheim@gmail.com');

				$this->email->cc('rianny@definite.co.id');
				$this->email->bcc('axa.report.dump@gmail.com');
				
				$this->email->subject('[Daily Report]-'.$row['nama_entity']);
				$this->email->message(
					'<p>Dear tim AXA mandiri,</p>'. 
					'<p>Terlampir database registran yang mendaftar melalui halaman Product Matrix.</br>'. 
					'<p>Mohon di follow up dalam 1x24 jam untuk tetap menjaga ketertarikan pendaftar</p>'.
					  
					'<p>Terima kasih</p>'.
					'<p>AXA Mandiri</p>'
				);
				$this->email->attach('/var/www/html/axamandiri/axamandiri_form/axamandiri_temp/DailyReport-'.$row['nama_entity'].'-'.date('d-m-Y').'.xls');
				if($this->email->send()) {
					$data3['daily'] = 1;
					$this->db->where('daily', 0);
					$this->db->where('product_matrix', $row['nama_entity']);
					$this->db->update('kontak', $data3);

					unlink('/var/www/html/axamandiri/axamandiri_form/axamandiri_temp/DailyReport-'.$row['nama_entity'].'-'.date('d-m-Y').'.xls');
				}else{
					echo "Email cannot send";
				}
				
				$count = 2;
			}
			else{
				$this->email->clear(TRUE);
				$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE));
				$this->email->from($row['email_from'],'AXA Mandiri');
				$this->email->to($row['email_to']);
				// $this->email->to('emilddurkheim@gmail.com');

				$this->email->cc('digital@axa.co.id');
				
				$this->email->bcc('axa.dump.report@gmail.com');
				$this->email->subject('[Daily Report]'.$row['nama_entity']);
				$this->email->message(
					'<p>Dear tim AXA,</p>'. 
						'<p>Data mulai tanggal '.date('j F Y', time() - 86400).' Jam 16:01 sampai hari ini tanggal '.date('j F Y ').' jam '.date('H:i').' kosong</p><br>'.
						'<p>Terima kasih</p>'.
						'<p>Digital AXA</p>'
				);
				$this->email->send();
			}
		}
	}
	function daily_sample(){
		date_default_timezone_set('Asia/Bangkok');

		$sql = "select * from entity_customer_care order by nama_entity asc";
		
		$query = $this->db->query($sql);
		$result = $query->result_array();

		foreach($result as $row)
		{
			$sql2 = "SELECT * from kontak where product_matrix = '".$row['nama_entity']."' and daily = 0 order by product_matrix asc";
			
			$query2 = $this->db->query($sql2);
			$result2 = $query2->result_array();
			$count = 2;
			
			if ($query2->num_rows() > 0)
			{	
				$this->email->clear(TRUE);
				$sheet = new PHPExcel();
				$sheet->setActiveSheetIndex(0);
				//name the worksheet
				$sheet->getActiveSheet()->setTitle($row['nama_entity']);
				$sheet->getActiveSheet()
								->setCellValue('A1', 'No')
								->setCellValue('B1', 'Nama Lengkap')
								->setCellValue('C1', 'No. Telepon')
								->setCellValue('D1', 'Email')
								->setCellValue('E1', 'Tgl. Lahir')
								->setCellValue('F1', 'Propinsi')
								->setCellValue('G1', 'Kota')
								->setCellValue('H1', 'Product Matrix')
								->setCellValue('I1', 'Waktu Submit');
							
				$sheet->getActiveSheet()->getColumnDimension('A')->setWidth(12);
				$sheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('D')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('E')->setWidth(32);
				$sheet->getActiveSheet()->getColumnDimension('F')->setWidth(32);
				$sheet->getActiveSheet()->getColumnDimension('G')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('H')->setWidth(30);
				$sheet->getActiveSheet()->getColumnDimension('I')->setWidth(20);

				$sheet->getActiveSheet()->getStyle('A')->getNumberFormat()->setFormatCode('0');
				
				
				$sheet->getActiveSheet()->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$sheet->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


				foreach($result2 as $row2)
				{
					$sheet->setActiveSheetIndex()->setCellValue("A".$count, $count-1);
					$sheet->setActiveSheetIndex()->setCellValue("B".$count, $row2['nama_lengkap']);
					$sheet->getActiveSheet()->setCellValueExplicit("C".$count,$row2['no_tlp'],PHPExcel_Cell_DataType::TYPE_STRING);
					$sheet->setActiveSheetIndex()->setCellValue("D".$count, $row2['email']);
					$sheet->setActiveSheetIndex()->setCellValue("E".$count, $row2['tgl_lahir']);
					$sheet->setActiveSheetIndex()->setCellValue("F".$count, $row2['propinsi']);
					$sheet->setActiveSheetIndex()->setCellValue("G".$count, $row2['kota']);
					$sheet->setActiveSheetIndex()->setCellValue("H".$count, $row2['product_matrix']);
					$sheet->setActiveSheetIndex()->setCellValue("I".$count, $row2['submit_time']);
					$count++;
				}

				$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');  
				
				//$objWriter->save('./axamandiri_temp/DailyReport'.$row['nama_entity'].'-'.date('d-m-Y').'.xls');
				$filename='Daily-'.$row['nama_entity'].'-'.date('d-m-Y').'.xls';
				// header('Content-type: application/ms-excel');
		  //       header("Content-Disposition: attachment; filename=\"".$filename); 
		  //       header("Cache-control: private");        
		        // $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		       $objWriter->save('axamandiri_temp/'.$filename); 
		       // header('Content-Disposition: attachment; filename='.$file);
				// $objWriter->save('php://output'); 
		        // $objWriter->save("axamandiri_temp/".$filename);
		        // header("location: ".base_url()."temp/$filename");
		        //unlink(base_url()."temp/$filename");

				$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE));
				$this->email->from($row['email']);
				// $this->email->to($row['email_from']); 
				$this->email->to($row['emilddurkheim@gmail.com']); 
				//$this->email->to('rianny.tambuwun@gmail.com , adhit@definite.co.id');
				// $this->email->cc('digital@axa.co.id');
				// $this->email->bcc('axa.report.dump@gmail.com');


				
				$this->email->subject('[Daily Report]'.$row['nama_entity']);
				$this->email->message(
					'<p>Dear tim AXA,</p>'. 
					'<p>Terlampir database registran yang mendaftar melalui halaman Product Matrix.</br>'. 
					'<p>Mohon di follow up dalam 1x24 jam untuk tetap menjaga ketertarikan pendaftar</p>'.
					  
					'<p>Terima kasih</p>'.
					'<p>Digital AXA</p>'
				);
				$this->email->attach('./axamandiri_temp/DailyReport'.$row['nama_entity'].'-'.date('d-m-Y').'.xls');
				if($this->email->send()) {
					$data3['daily'] = 1;
					$this->db->where('daily', 0);
					$this->db->where('product_matrix', $row['nama_entity']);
					$this->db->update('kontak', $data3);

					unlink('./axamandiri_temp/DailyReport'.$row['nama_entity'].'-'.date('d-m-Y').'.xls');
				}else{
					echo "Email cannot send";
				}
				
				$count = 2;
			}
			else{
				$this->email->clear(TRUE);
				$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE));
				$this->email->from('noreply@axa.co.id');
				// $this->email->cc('digital@axa.co.id');
				$this->email->to('emilddurkheim@gmail.com');

				
				$this->email->bcc('axa.dump.report@gmail.com');
				$this->email->subject('[Daily Report]'.$row['nama_entity']);
				$this->email->message(
					'<p>Dear tim AXA,</p>'. 
						'<p>Data mulai tanggal '.date('j F Y', time() - 86400).' Jam 16:01 sampai hari ini tanggal '.date('j F Y ').' jam '.date('H:i').' kosong</p><br>'.
						'<p>Terima kasih</p>'.
						'<p>Digital AXA</p>'
				);
				$this->email->send();
			}
		}
	}

}