<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hubungi_kami extends Ci_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Hubungi_kami_model');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->library('email');
		$this->load->library('excel');
	}

	function index(){		
		//$this->load->view('hubungi_kami_view');
	}

	function submit_data(){
	date_default_timezone_set('Asia/Bangkok');

		$data = array('nama' => $this->input->post('nama', TRUE),
					'tgl_lahir' => $this->input->post('tgl_lahir', TRUE),
					'email' => $this->input->post('email', TRUE),
					'alamat' => $this->input->post('alamat', TRUE),
					'no_hp' => $this->input->post('no_hp', TRUE),	
					'no_tlp_kantor' => $this->input->post('no_tlp_kantor',TRUE),					
					'no_tlp' => $this->input->post('no_tlp',TRUE),					
					'fax' => $this->input->post('fax',TRUE),					
					'entity' => $this->input->post('entity',TRUE),					
					'produk' => $this->input->post('produk',TRUE),					
					'no_polis' => $this->input->post('no_polis', TRUE),
					'kategori' => $this->input->post('kategori', TRUE),
					'subjek' => $this->input->post('subjek', TRUE),
					'pesan' => $this->input->post('pesan', TRUE),
					'submit_time' => date('Y-m-d/H:i:s'),							
					'banner_source' => $this->input->post('banner_source', TRUE),
					'utm_source' => $this->input->post('utm_source', TRUE),
					'utm_medium' => $this->input->post('utm_medium', TRUE),
					'utm_term' => $this->input->post('utm_term', TRUE),
					'utm_content' => $this->input->post('utm_content', TRUE),
					'utm_campaign' => $this->input->post('utm_campaign', TRUE),
					'gclid' => $this->input->post('gclid', TRUE),
					
					);
				
		$this->hubungi_kami_model->insertData('hubungi_kami',$data);
		/*kirim email*/
		$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE, 'priority' => 1));
		
		if ($this->input->post('entity')=='amfs') {
			$this->email->from('noreply.life@cc.axa-mandiri.co.id', 'AXA Mandiri');
		}
		elseif($this->input->post('entity')=='amfs'){
			$this->email->from('noreply.general@cc.axa-mandiri.co.id', 'AXA Mandiri');
		}
		
		$this->email->to($this->input->post('email')); 
		$this->email->subject('Terima kasih telah menghubungi AXA Mandiri');
		$this->email->message(
			'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'.
			'<html>'.
			    '<head>'.
			        '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'.
			        '<!-- Facebook sharing information tags -->'.
			        '<meta property="og:title" content="">'.
			        '<title>Selamat Datang di Dunia Kesehatan tanpa Hambatan</title>'.
				'<style type="text/css">'.
					'#outlook a{'.
						'padding:0;'.
					'}'.
					'body{'.
						'width:100% !important;'.
					'}'.
					'.ReadMsgBody{'.
						'width:100%;'.
					'}'.
					'.ExternalClass{'.
						'width:100%;'.
					'}'.
					'body{'.
						'-webkit-text-size-adjust:none;'.
					'}'.
					'body{'.
						'margin:0;'.
						'padding:0;'.
					'}'.
					'img{'.
						'border:0;'.
						'height:auto;'.
						'line-height:100%;'.
						'outline:none;'.
						'text-decoration:none;'.
					'}'.
					'table td{'.
						'border-collapse:collapse;'.
					'}'.
					'#backgroundTable{'.
						'height:100% !important;'.
						'margin:0;'.
						'padding:0;'.
						'width:100% !important;'.
					'}'.
					'body,#backgroundTable{'.
						'background-color:#ffffff;'.
					'}'.
					'#templateContainer{'.
						'border:1px solid #DDDDDD;'.
					'}'.
					'h1,.h1{'.
						'color:#202020;'.
						'display:block;'.
						'font-family:Arial;'.
						'font-size:34px;'.
						'font-weight:bold;'.
						'line-height:100%;'.
						'margin-top:0;'.
						'margin-right:0;'.
						'margin-bottom:10px;'.
						'margin-left:0;'.
						'text-align:left;'.
					'}'.
					'h2,.h2{'.
						'color:#202020;'.
						'display:block;'.
						'font-family:Arial;'.
						'font-size:30px;'.
						'font-weight:bold;'.
						'line-height:100%;'.
						'margin-top:0;'.
						'margin-right:0;'.
						'margin-bottom:10px;'.
						'margin-left:0;'.
						'text-align:left;'.
					'}'.
					'h3,.h3{'.
						'color:#202020;'.
						'display:block;'.
						'font-family:Arial;'.
						'font-size:26px;'.
						'font-weight:bold;'.
						'line-height:100%;'.
						'margin-top:0;'.
						'margin-right:0;'.
						'margin-bottom:10px;'.
						'margin-left:0;'.
						'text-align:left;'.
					'}'.
					'h4,.h4{'.
						'color:#202020;'.
						'display:block;'.
						'font-family:Arial;'.
						'font-size:22px;'.
						'font-weight:bold;'.
						'line-height:100%;'.
						'margin-top:0;'.
						'margin-right:0;'.
						'margin-bottom:10px;'.
						'margin-left:0;'.
						'text-align:left;'.
					'}'.
					'#templatePreheader{'.
						'background-color:#FAFAFA;'.
					'}'.
					'.preheaderContent div{'.
						'color:#505050;'.
						'font-family:Arial;'.
						'font-size:10px;'.
						'line-height:100%;'.
						'text-align:left;'.
					'}'.
					'.preheaderContent div a:link,.preheaderContent div a:visited,.preheaderContent div a .yshortcuts {'.
						'color:#336699;'.
						'font-weight:normal;'.
						'text-decoration:underline;'.
					'}'.
					'#templateHeader{'.
						'background-color:#FFFFFF;'.
						'border-bottom:0;'.
					'}'.
					'.headerContent{'.
						'color:#202020;'.
						'font-family:Arial;'.
						'font-size:34px;'.
						'font-weight:bold;'.
						'line-height:100%;'.
						'padding:0;'.
						'text-align:center;'.
						'vertical-align:middle;'.
					'}'.
					'.headerContent a:link,.headerContent a:visited,.headerContent a .yshortcuts {'.
						'color:#336699;'.
						'font-weight:normal;'.
						'text-decoration:underline;'.
					'}'.
					'#headerImage{'.
						'height:auto;'.
						'max-width:600px !important;'.
					'}'.
					'#templateContainer,.bodyContent{'.
						'background-color:#FFFFFF;'.
					'}'.
					'.bodyContent div{'.
						'color:#505050;'.
						'font-family:Arial;'.
						'font-size:14px;'.
						'line-height:150%;'.
						'text-align:left;'.
					'}'.
					'.bodyContent div a:link,.bodyContent div a:visited,.bodyContent div a .yshortcuts {'.
						'color:#336699;'.
						'font-weight:normal;'.
						'text-decoration:underline;'.
					'}'.
					'.bodyContent img{'.
						'display:inline;'.
						'height:auto;'.
					'}'.
					'#templateFooter{'.
						'background-color:#FFFFFF;'.
						'border-top:0;'.
					'}'.
					'.footerContent div{'.
						'color:#707070;'.
						'font-family:Arial;'.
						'font-size:12px;'.
						'line-height:125%;'.
						'text-align:left;'.
					'}'.
					'.footerContent div a:link,.footerContent div a:visited,.footerContent div a .yshortcuts {'.
						'color:#336699;'.
						'font-weight:normal;'.
						'text-decoration:underline;'.
					'}'.
					'.footerContent img{'.
						'display:inline;'.
					'}'.
					'#social{'.
						'background-color:#FAFAFA;'.
						'border:0;'.
					'}'.
					'#social div{'.
						'text-align:center;'.
					'}'.
					'#utility{'.
						'background-color:#FFFFFF;'.
						'border:0;'.
					'}'.
					'#utility div{'.
						'text-align:center;'.
					'}'.
					'#monkeyRewards img{'.
						'max-width:190px;'.
					'}'.
			'</style></head>'.
			    '<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #ffffff;width: 100% !important;">'.
			    	'<center>'.
			        	'<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable" style="margin: 0;padding: 0;background-color: #ffffff;height: 100% !important;width: 100% !important;">'.
			            	'<tr>'.
			                	'<td align="center" valign="top" style="border-collapse: collapse;">'.
			                        '<!-- // Begin Template Preheader \\ -->'.
			                        '<table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader" style="background-color: #FFFFFF;">'.
			                            '<tr>'.
			                            	'<td></td>'.
			                            '</tr>'.
			                        '</table>'.
			                        '<!-- // End Template Preheader \\ -->'.
			                    	'<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer" style="border: 1px solid #FFFFFF;background-color: #FFFFFF;">'.
			                        	'<tr>'.
			                            	'<td align="center" valign="top" style="border-collapse: collapse;">'.
			                                    '<!-- // Begin Template Body \\ -->'.
			                                	'<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">'.
			                                    	'<tr>'.
			                                            '<td valign="top" class="bodyContent" style="border-collapse: collapse;background-color: #FFFFFF;">'.					                                                '<!-- // Begin Module: Standard Content \\ -->'.
			                                                '<table border="0" cellpadding="40" cellspacing="0" width="100%">'.
			                                                    '<tr>'.
			                                                        '<td valign="top" style="border-collapse: collapse;">'.
			                                                            '<div style="color: #505050;font-family: Arial;font-size: 14px;line-height: 150%;text-align: left;"><span style="color:#003399; font-size:14px;">Dear '.$_POST['nama'].',<br>'.
																	'<br>'.
																	'Terima kasih telah menghubungi kami<br>'.
																	'<br>'.
																	
																	'<br>'.
																	'Salam,<br>'.
																	'Tim AXA Mandiri<br>'.
																	'<br>'.
																'</td>'.
			                                                    '</tr>'.
			                                                '</table>'.

			                                                '<!-- // End Module: Standard Content \\ -->'.
			                                            '</td>'.
			                                        '</tr>'.
			                                    '</table>'.
			                                    '<!-- // End Template Body \\ -->'.
			                                '</td>'.
			                            '</tr>'.
			                        	'<tr>'.
			                            	'<td align="center" valign="top" style="border-collapse: collapse;">'.
			                                '</td>'.
			                            '</tr>'.
			                        '</table>'.
			                        '<br>'.
			                    '</td>'.
			                '</tr>'.
			            '</table>'.
			        '</center>'.
			    '</body>'.
			'</html>'
				);

		$this->email->send(); 


		

		$this->email->clear(TRUE);
		$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE));
		if ($this->input->post('entity')=='amfs') {
			$this->email->from('noreply.life@cc.axa-mandiri.co.id', 'AXA Mandiri');
		}
		elseif($this->input->post('entity')=='agi'){
			$this->email->from('noreply.general@cc.axa-mandiri.co.id', 'AXA Mandiri');
		}
		$this->email->to('emilddurkheim@gmail.com','rianny@definite.co.id');
		// $this->email->cc('said@definite.co.id');
		$this->email->to('customer@axa-mandiri.co.id');
		$this->email->bcc('axa.report.dump@gmail.com'); 

		$this->email->subject('[Contact Us] '.$this->input->post('subjek'));
		$this->email->message(
			'<p>Dear tim AXA Mandiri,</p>'. 
			'<p>Email From 		: '.$this->input->post('email').'</p>'.
			'<p>Email Subjek 	: '.$this->input->post('subjek').'</p>'.
			'<p>Kategori     	: '.$this->input->post('kategori').'</p>'.
			'<p>Jenis Entity   	: '.$this->input->post('entity').'</p>'.
			'<p>Nama Produk   	: '.$this->input->post('produk').'</p>'.
			'<p>Nama Lengkap 	: '.$this->input->post('nama').'</p>'.
			'<p>Alamat 	   	 	: '.$this->input->post('alamat').'</p>'.
			'<p>Tgl Lahir 	 	: '.$this->input->post('tgl_lahir').'</p>'.
			'<p>No Handphone 	: '.$this->input->post('no_hp').'</p>'.
			'<p>No Tlp       	: '.$this->input->post('no_tlp').'</p>'.
			'<p>Fax       		: '.$this->input->post('fax').'</p>'.
			'<p>No Tlp Kantor   : '.$this->input->post('no_tlp_kantor').'</p>'.
			'<p>No Polis     	: '.$this->input->post('no_polis').'</p>'.
			'</br>'.
			'</br>'.
			'<p>Pesan : </p>'.
			'<p><i>"'.
			$this->input->post('pesan').
			'"</i></p>'.
			
			  
			'<p>Terima kasih</p>'.
			'<p>Salam</p>'
		);
		$this->email->send();

		
		?>
			<script> window.location = "<?php echo base_url(); ?>hubungi-kami"; </script>
		<?php

	}
	function daily(){
	date_default_timezone_set('Asia/Bangkok');

		/*********************************Generate DATA HUBUNGI KAMI EXCEL***********************************/ 
		/*excel*/
		$sql = "SELECT * FROM hubungi_kami where daily=0 ";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		$count = 2;
		if($query->num_rows()>0)
		{	
				$this->excel->setActiveSheetIndex(0);
				//name the worksheet
				$this->excel->getActiveSheet()->setTitle('Daftar Contact Us');
				$this->excel->getActiveSheet()
								->setCellValue('A1', 'No.')
								->setCellValue('B1', 'Nama')
								->setCellValue('C1', 'Tgl. Lahir')
								->setCellValue('D1', 'Email')
								->setCellValue('E1', 'Alamat')
								->setCellValue('F1', 'No. Handphone')
								->setCellValue('G1', 'No. Tlp Kantor')
								->setCellValue('H1', 'No. Tlp Rumah')
								->setCellValue('I1', 'Fax')
								->setCellValue('J1', 'Entity')
								->setCellValue('K1', 'Produk')
								->setCellValue('L1', 'No. Polis')
								->setCellValue('M1', 'Kategori')
								->setCellValue('N1', 'Subjek')
								->setCellValue('O1', 'Pesan')
								->setCellValue('P1', 'Waktu Submit');


				$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
				$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
				$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
				$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
				$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(32);
				$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(32);
				$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(32);
				$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(32);
				$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(32);
				$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(32);
				$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
				$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(32);
				$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(32);
				$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(32);
				$this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(60);
				$this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(32);

				$this->excel->getActiveSheet()->getStyle('A')->getNumberFormat()->setFormatCode('0');
				
				$this->excel->getActiveSheet()->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				
				foreach($result as $row)
				{
					$this->excel->setActiveSheetIndex()->setCellValue("A".$count, $count-1);
					$this->excel->setActiveSheetIndex()->setCellValue("B".$count, $row['nama']);
					$this->excel->setActiveSheetIndex()->setCellValue("C".$count, $row['tgl_lahir']);
					$this->excel->setActiveSheetIndex()->setCellValue("D".$count, $row['email']);
					$this->excel->setActiveSheetIndex()->setCellValue("E".$count, $row['alamat']);
					$this->excel->getActiveSheet()->setCellValueExplicit("F".$count,$row['no_hp'],PHPExcel_Cell_DataType::TYPE_STRING);
					$this->excel->getActiveSheet()->setCellValueExplicit("G".$count,$row['no_tlp_kantor'],PHPExcel_Cell_DataType::TYPE_STRING);
					$this->excel->getActiveSheet()->setCellValueExplicit("H".$count,$row['no_tlp'],PHPExcel_Cell_DataType::TYPE_STRING);
					$this->excel->getActiveSheet()->setCellValueExplicit("I".$count,$row['fax'],PHPExcel_Cell_DataType::TYPE_STRING);
					$this->excel->setActiveSheetIndex()->setCellValue("J".$count, $row['entity']);
					$this->excel->setActiveSheetIndex()->setCellValue("K".$count, $row['produk']);
					$this->excel->getActiveSheet()->setCellValueExplicit("L".$count,$row['no_polis'],PHPExcel_Cell_DataType::TYPE_STRING);
					$this->excel->setActiveSheetIndex()->setCellValue("M".$count, $row['kategori']);
					$this->excel->setActiveSheetIndex()->setCellValue("N".$count, $row['subjek']);
					$this->excel->setActiveSheetIndex()->setCellValue("O".$count, $row['pesan']);
					$this->excel->setActiveSheetIndex()->setCellValue("P".$count, $row['submit_time']);
					
					$count++;
				}
				$count = 2;

				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
				
				$objWriter->save('/var/www/html/axamandiri/axamandiri_form/axamandiri_temp/DailyReport_Contact_Us'.date('d-m-Y').'.xls');

				/*KIRIM SELURUH DATA EXCEL*/
				$this->email->clear(TRUE);
				$this->email->initialize(array('mailtype' => 'html', 'validate' => TRUE));
				$this->email->from('noreply.general@cc.axa-mandiri.co.id');
				// $this->email->to('pamungkas1992@gmail.com');
				// $this->email->cc('rianny@definite.co.id, emild@definite.co.id, dimas@definite.co.id');
				$this->email->to('emilddurkheim@gmail.com');
				$this->email->cc('');
				$this->email->bcc('axa.report.dump@gmail.com'); 

				$this->email->subject("[Hubungi Kami] Data Report");
				$this->email->message(
					'<p>Dear tim AXA Mandiri,</p>'. 
					'<p>Terlampir data yang mendaftar melalui halaman Hubungi Kami'.
					' secara online.</br>'. 
					'<p>Mohon di follow up dalam 1x24 jam untuk tetap menjaga ketertarikan klien</p>'.
					  
					'<p>Terima kasih</p>'.
					'<p>Digital AXA Mandiri</p>'
				);
				$this->email->attach('/var/www/html/axamandiri/axamandiri_form/axamandiri_temp/DailyReport_Contact_Us'.date('d-m-Y').'.xls');
				if($this->email->send()) {
					$data['daily'] = 1;
					$this->db->where('daily', 0);
					$this->db->update('hubungi_kami', $data);

					// unlink('/var/www/html/axamandiri/axamandiri_form/axamandiri_temp/DailyReport_Contact_Us'.date('d-m-Y').'.xls');
				}else{
					echo "Email cannot send";
				}
		}else{
			$this->email->clear(TRUE);
			$this->email->initialize(array('mailtype'=>'html','validate'=>TRUE));
			$this->email->from('AXA Mandiri','noreply@axa-mandiri.co.id');
			// $this->email->to('customer@axa-mandiri.co.id');
			$this->email->to('emilddurkheim@gmail.com');
			$this->email->cc('digital@axa.co.id');
			
			$this->email->bcc('axa.dump.report@gmail.com');
			$this->email->subject("[Hubungi Kami] Data Report");
			$this->email->message(
				'<p>Dear tim AXA,</p>'. 
					'<p>Data mulai tanggal '.date('j F Y', time() - 86400).' Jam 18:01 sampai hari ini tanggal '.date('j F Y ').' jam '.date('H:i').' kosong</p><br>'.
					'<p>Terima kasih</p>'.
					'<p>Digital AXA</p>'
			);
			$this->email->send();
		}

	}

}