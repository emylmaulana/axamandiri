<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Kontak_model extends CI_Model {

function __construct() {
		parent::__construct();
	}
		
	function add($data) {
		$this->db->set($data);
		$this->db->insert('kontak');
		return $this->db->insert_id();
	}
	function insertData($table,$data)
	{
		$this->db->insert($table,$data);
	}

}