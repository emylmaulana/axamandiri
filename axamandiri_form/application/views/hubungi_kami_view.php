
<html>
<head>
	<title>Hubungi Kami</title>
</head>
<body>
		<strong>Hubungi kami</strong>
		<p align="left">Jika Anda memiliki pertanyaan, keluhan, saran atau butuh layanan pelanggan, lengkapi formulir ini dan kami akan menghubungi Anda.</p>

		<form action="<?php echo base_url('hubungi_kami/submit_data')?>" method="post">
			<table border="0">
				<tr>
					<td>Nama*</td>
					<td>
						<input type="text" name="nama">
					</td>
				</tr>
				<tr>
					<td>Tanggal Lahir*</td>
					<td>
						<input type="date" name="tgl_lahir">
					</td>
				</tr>
				<tr>
					<td>email*</td>
					<td>
						<input type="text" name="email">
					</td>
				</tr>
				<tr>
					<td>Alamat*</td>
					<td>
						<input type="text" name="alamat">
					</td>
				</tr>
				<tr>
					<td>No. HP*</td>
					<td>
						<input type="text" name="no_hp">
					</td>
				</tr>
				<tr>
					<td>No. Tlp Rumah*</td>
					<td>
						<input type="text" name="no_tlp">
					</td>
				</tr>
				<tr>
					<td>No. Polis (jika ada)</td>
					<td>
						<input type="text" name="no_polis">
					</td>
				</tr>
				<tr>
					<td>No. kategori*</td>
					<td>
						<select name="kategori" class="wpcf7-form-control wpcf7-select" aria-invalid="false">
							<option value="">---</option>
							<option value="Pertanyaan Umum">Pertanyaan Umum</option>
							<option value="Informasi Produk">Informasi Produk</option>
							<option value="Informasi Financial Advisor">Informasi Financial Advisor</option>
							<option value="Saran">Saran</option>
							<option value="Keluhan">Keluhan</option>
							<option value="Lainnya">Lainnya</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Subjek*</td>
					<td>
						<input type="text" name="subjek">
					</td>
				</tr>
				<tr>
					<td>Pesan Anda*</td>
					<td>
						<textarea name="pesan" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="submit" value="Kirim">
					</td>
				</tr>
			</table>
		</form>
</body>
</html>