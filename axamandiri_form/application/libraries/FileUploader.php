<?php

class FileUploader {
    private $allowedExtensions = array();
    private $sizeLimit = 10485760;
    private $file;

    function __construct($params){
    	if (!empty($params))
			extract($params);
		
		if (empty($allowedExtensions))
			$allowedExtensions = array();
		if (empty($sizeLimit))
			$sizeLimit = 10485760;
		
        $allowedExtensions = array_map("strtolower", $allowedExtensions);
            
        $this->allowedExtensions = $allowedExtensions;        
        $this->sizeLimit = $sizeLimit;
        
        $this->checkServerSettings();       

        if (isset($_GET['qqfile'])) {
            $this->file = new UploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
            $this->file = new UploadedFileForm();
        } else {
            $this->file = false; 
        }
    }
    
    private function checkServerSettings(){        
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));
        log_message('info', 'FileUploader | Post Size: '.$postSize.', Upload Size:'.$uploadSize.', Limit Size:'.$this->sizeLimit);
        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';             
            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");    
        }        
    }
    
    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;        
        }
        return $val;
    }
    
    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     */
    public function handleUpload($uploadDirectory, $newname = '', $replaceOldFile = FALSE){
        if (!is_writable($uploadDirectory)){
            return array('status' => 'error', 'error' => "Server error. Upload directory isn't writable.");
        }
        
        if (!$this->file){
            return array('status' => 'error', 'error' => 'No files were uploaded.');
        }
        
        $size = $this->file->getSize();
        
        if ($size == 0) {
            return array('status' => 'error', 'error' => 'File is empty');
        }
        
        if ($size > $this->sizeLimit) {
            return array('status' => 'error', 'error' => 'File is too large');
        }
        
        $pathinfo = pathinfo($this->file->getName());
        $filename = !empty($newname) ? $newname : $pathinfo['filename'];
        $ext = $pathinfo['extension'];

        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
            $these = implode(', ', $this->allowedExtensions);
            return array('status' => 'error', 'error' => 'File has an invalid extension, it should be one of '. $these . '.');
        }
        
        if(!$replaceOldFile){
            /// don't overwrite previous files that were uploaded
            while (file_exists($uploadDirectory . $filename . '.' . $ext)) {
                $filename .= rand(10, 99);
            }
        }
        
        if ($this->file->save($uploadDirectory . $filename . '.' . $ext)){
        	$this->file->setName($filename.'.'.$ext);
            return array('status' => 'success', 'success'=> true, 'filename' => $this->file->getName());
        } else {
            return array('status' => 'error', 'error'=> 'Could not save uploaded file.' .
                'The upload was cancelled, or server error encountered');
        }
    }
	public function getName() {
		if ($this->file) {
			return $this->file->getName();
		}
	}
}

/**
 * Handle file uploads via XMLHttpRequest
 */
class UploadedFileXhr {
	private $filename = '';
	
	function __construct() {
		$this->filename = $_GET['qqfile'];
	}
	
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {    
        $input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);
        
        if ($realSize != $this->getSize()){            
            return false;
        }
        
        $target = fopen($path, "w");        
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);
        
        return true;
    }
	
	function setName($name) {
		$this->filename = $name;
	}
	
    function getName() {
        return $this->filename;
    }
	
    function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])){
            return (int)$_SERVER["CONTENT_LENGTH"];            
        } else {
            throw new Exception('Getting content length is not supported.');
        }      
    }   
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class UploadedFileForm {
	private $filename = '';
	private $filesize = 0;
	
	function __construct() {
		$this->filename = $_FILES['qqfile']['name'];
		$this->filesize = $_FILES['qqfile']['size'];
	}  
	
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {
        if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
            return false;
        }
        return true;
    }
	function setName($name) {
		$this->filename = $name;
	}
    function getName() {
        return $this->filename;
    }
    function getSize() {
        return $this->filesize;
    }
}
