
<?php/** * Template Name: Karir: Agent */?>
<?php get_header();?>

<?php
	//session_start();
	$cap = 'notEq';
	//session_start();
	$ranStr = md5(microtime());
	$ranStr = substr($ranStr, 0, 6);
	
	// $this->session->set_userdata('cap_code', $ranStr);
	$_SESSION['cap_code']=$ranStr;
	$newImage = imagecreatefromjpeg(get_bloginfo('template_url').'/images/cap_bg.jpg');
	$txtColor = imagecolorallocate($newImage, 0, 0, 0);
	imagestring($newImage, 5, 5, 5, $ranStr, $txtColor);
	//header("Content-type: image/jpeg");
	// imagejpeg($newImage, get_bloginfo('template_url').'/images/captcha.jpg');
	//imagejpeg($newImage, '/Applications/XAMPP/xamppfiles/htdocs/axaone/wp-content/themes/axaindonesia/images/captcha.jpg');
	imagejpeg($newImage, get_theme_root().'/axamandiri/images/captcha.jpg');
	
?>
<?php $_GET['status'];?>
<div id="page-container">
	<div id="masthead" class="row relative p-all-0">
		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
		<?php endif; ?>
		<div id="page-head" class="relative m-top-150" style="background:#fcfdff url(<?php echo $image[0]; ?>) no-repeat top right">
			<!-- <div class="separate-left-product absolute"></div> -->
			<div class="box absolute set-1 large-5">
				<?php $category = wp_get_post_terms($post->ID, 'matrix_category', array("fields" => "names")); ?>
				<p class="c-white f-18"><?php echo $category[1];?></p>
				

				<!-- <h1 class="f-45 lh-1"><?php the_title();?></h1> -->

				<div class="addthis_toolbox addthis_default_style addthis_16x16_style share-buttons left" style="width:180px;">
					<a class="addthis_button_facebook icons facebook fa fa-facebook"></a>
					<a class="addthis_button_twitter icons twitter fa fa-twitter"></a>
					<a class="addthis_button_print icons print fa fa-print"></a>
					<a class="addthis_button_email icons email fa fa-envelope"></a>
				</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
					<script type="text/javascript"></script>
				
				<!--<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e88dff717990fa"></script>-->
				<!-- AddThis Button END -->
			</div>

			<div class="separate-right-product absolute"></div>
		</div>
	</div><!--end masthead-->
	<div class="row p-all-0">
	<div id="text-top-solusi" class=" section white clearfix">
		<div class="bghorizontal-yellow large-6 columns p-left-0">
			<h4 class="c-white"> <span class="c-bluemandiri"><?php the_title(); ?></span></h4>
		</div>

		<div class="large-4 columns">

			<div id="social-header-content" class="clearfix" style="clear:both;">
                <!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style addthis_16x16_style share-buttons right" style="width:185px;" addthis:description="Temukan solusi perlindungan yang tepat sesuai kebutuhan Anda di AXA Mandiri"  addthis:title="Temukan solusi perlindungan yang tepat sesuai kebutuhan Anda di AXA Mandiri">
					<a class="addthis_button_facebook icons facebook fa fa-facebook"></a>
					<a class="addthis_button_twitter icons twitter fa fa-twitter"></a>
					<a class="addthis_button_print icons print fa fa-print"></a>
					<a class="addthis_button_email icons email fa fa-envelope"></a>
				</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e88dff717990fa"></script>
			</div>
		</div>

	</div>
	<div id="page-box" class="sections white clearfix">
		<section id="body-1">
			<h3 class="uppercase fw-normal f-17"><?php _e("<!--:en-->Introduction<!--:--><!--:id-->Pengantar<!--:-->"); ?></h3>
			<h4 class="c-blue f-24"><?php the_field('sub_title');?></h4>
			<div class="column-2 f-16 c-greylight">
			<?php if( have_posts() ) : the_post(); ?>
				 <?php the_content();?>
			<?php endif;?>
			</div>
		</section>
	</div>

	<div id="cover-manfaat" class="background-vector clearfix">
		<section id="manfaat" class="bg-cover-blue sections clearfix">
			<h3 class="c-white fw-normal uppercase f-17"><?php _e("<!--:en-->Benefit<!--:--><!--:id-->Manfaat<!--:-->"); ?></h3>
			<h4 class="c-white m-bottom-45"><?php the_field('manfaat_sub_title');?></h4>
			<ul id="manfaat-slide">
				<?php $i = 1; while(has_sub_field('matrix_manfaat')): ?>
					<li class="text-center elements <?php if(($i % 2) == 0) echo 'dark'; ?>">
						<div class="box p-all-20">
							<img class="icon-manfaat" src="<?php the_sub_field("icon");?>">
							<p><strong><?php the_sub_field("title");?></strong></p>
							<?php the_sub_field("body");?>
						</div>
					</li>
				<?php $i++; endwhile;?>
			</ul>
		</section>
		<?php# if(get_field('matrix_syarat2')!=""):?>
		<section id="syarat" class="sections relative o-hidden">
			<div class="relative" style="z-index:1000;">
			<h3 class="fw-normal f-17 uppercase m-bottom-40">Syarat &amp; Ketentuan</h3>
			<ul id="list-syarat" class="small-block-grid-1 medium-block-grid-2 large-block-grid-2 clearfix">
				<?php while (has_sub_field('matrix_syarat2')):?>
					<?php if(get_sub_field('usia_masuk')!=""):?>
					<li class="usia-masuk">
						<div class="bg-white o-hidden radius-all-5">
							<div class="icon-125x100">
								<!-- <span class="icon-solusi" style="background: url(<?php the_sub_field('icon_usia_masuk');?>) right no-repeat;"></span> -->
								<img src="<?php the_sub_field('icon_usia_masuk');?>" class="icon-solusi">
							</div>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong>Usia Masuk</strong></p>
									<p class="f-16"><?php the_sub_field('usia_masuk');?></p>
								</div>
						</div>
					</li>
					<?php endif;?>
					<?php if(get_sub_field('masa_pertanggungan')!=""):?>
					<li class="masa_pertanggungan">
						<div class="bg-white o-hidden radius-all-5">
							<div class="icon-125x100">
								<img src="<?php the_sub_field('icon_masa_pertanggungan');?>" class="icon-solusi">
							</div>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong>Masa Pertanggungan</strong></p>
									<p class="f-16"><?php the_sub_field('masa_pertanggungan');?></p>
								</div>
						</div>
					</li>
					<?php endif;?>
					<?php if(get_sub_field('mata_uang')!=""):?>
					<li class="mata_uang">
						<div class="bg-white o-hidden radius-all-5">
							<div class="icon-125x100">
								<img src="<?php the_sub_field('icon_mata_uang');?>" class="icon-solusi">
							</div>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong>Mata uang</strong></p>
									<p class="f-16"><?php the_sub_field('mata_uang');?></p>
								</div>
						</div>
					</li>
					<?php endif;?>
					<?php if(get_sub_field('minimum_premi')!=""):?>
					<li class="minimum_premi">
						<div class="bg-white o-hidden radius-all-5">
							<div class="icon-125x100">
								<img src="<?php the_sub_field('icon_minimum_premi');?>" class="icon-solusi">
							</div>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong>Minimum Premi</strong></p>
									<p class="f-16"><?php the_sub_field('minimum_premi');?></p>
								</div>
						</div>
					</li>
					<?php endif;?>
					<?php if(get_sub_field('pengembalian_premi')!=""):?>
					<li class="pengembalian_premi">
						<div class="bg-white o-hidden radius-all-5">
							<div class="icon-125x100">
								<!-- <span class="icon-solusi" style="background: url(<?php the_sub_field('icon_pengambilan_premi');?>) right no-repeat;"></span> -->
								<img src="<?php the_sub_field('icon_pengambilan_premi');?>" class="icon-solusi">
							</div>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong>Pengambilan Premi</strong></p>
									<p class="f-16"><?php the_sub_field('pengembalian_premi');?></p>
								</div>
						</div>
					</li>
					<?php endif;?>
					<?php if(get_sub_field('cara_bayar')!=""):?>
					<li class="cara_bayar">
						<div class="bg-white o-hidden radius-all-5">
							<div class="icon-125x100">
								<!-- <span class="icon-solusi" style="background: url(<?php the_sub_field('icon_cara_bayar');?>) right no-repeat;"></span> -->
								<img src="<?php the_sub_field('icon_cara_bayar');?>" class="icon-solusi">
							</div>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong>Cara Bayar</strong></p>
									<p class="f-16"><?php the_sub_field('cara_bayar');?></p>
								</div>
						</div>
					</li>
					<?php endif;?>
					<?php if(get_sub_field('family_discount')!=""):?>
					<li class="family_discount">
						<div class="bg-white o-hidden radius-all-5">
							<div class="icon-125x100">
								<!-- <span class="icon-solusi" style="background: url(<?php the_sub_field('icon_family_discount');?>) right no-repeat;"></span> -->
								<img src="<?php the_sub_field('icon_family_discount');?>" class="icon-solusi">
							</div>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong>Family Discount</strong></p>
									<p class="f-16"><?php the_sub_field('family_discount');?></p>
								</div>
						</div>
					</li>
					<?php endif;?>
					<?php if(get_sub_field('jalur_distribusi')!=""):?>
					<li class="jalur_distribusi">
						<div class="bg-white o-hidden radius-all-5">
							<div class="icon-125x100">
								<!-- <span class="icon-solusi" style="background: url(<?php the_sub_field('icon_jalur_distribusi');?>) right no-repeat;"></span> -->
								<img src="<?php the_sub_field('icon_jalur_distribusi');?>" class="icon-solusi">
							</div>
								<div class="details">
									<p class="uppercase f-18 m-bottom-0"><strong>Jalur Distribusi</strong></p>
									<p class="f-16"><?php the_sub_field('jalur_distribusi');?></p>
								</div>
						</div>
					</li>
					<?php endif;?>
				<?php endwhile;?>
				<?php while (has_sub_field('matrix_syarat_aami')):?>
					<?php if(get_sub_field('pembelian_awal')!=""):?>
						<li class="pembelian_awal">
							<div class="bg-white o-hidden radius-all-5">
								<span class="icon-125x100"></span>
									<div class="details">
										<p class="uppercase f-18 m-bottom-0"><strong><?php _e("<!--:en-->Initial Investment<!--:--><!--:id-->Pembelian Awal<!--:-->"); ?></strong></p>
										<p class="f-16"><?php the_sub_field('pembelian_awal');?></p>
									</div>
							</div>
						</li>
					<?php endif;?>
					<?php if(get_sub_field('pembelian_selanjutnya')!=""):?>
						<li class="pembelian_selanjutnya">
							<div class="bg-white o-hidden radius-all-5">
								<span class="icon-125x100"></span>
									<div class="details">
										<p class="uppercase f-18 m-bottom-0"><strong><?php _e("<!--:en-->Top-up<!--:--><!--:id-->Pembelian selanjutnya<!--:-->"); ?></strong></p>
										<p class="f-16"><?php the_sub_field('pembelian_selanjutnya');?></p>
									</div>
							</div>
						</li>
					<?php endif;?>
					<?php if(get_sub_field('minimum_penjualan_kembali')!=""):?>
						<li class="minimum_penjualan_kembali">
							<div class="bg-white o-hidden radius-all-5">
								<span class="icon-125x100"></span>
									<div class="details">
										<p class="uppercase f-18 m-bottom-0"><strong><?php _e("<!--:en-->Minimum Redemption<!--:--><!--:id-->Minimum penjualan kembali<!--:-->"); ?></strong></p>
										<p class="f-16"><?php the_sub_field('minimum_penjualan_kembali');?></p>
									</div>
							</div>
						</li>
					<?php endif;?>
					<?php if(get_sub_field('minimum_saldo_kepemilikan_unit')!=""):?>
						<li class="minimum_saldo_kepemilikan_unit">
							<div class="bg-white o-hidden radius-all-5">
								<span class="icon-125x100"></span>
									<div class="details">
										<p class="uppercase f-18 m-bottom-0"><strong><?php _e("<!--:en-->Minimum Balance of Ownership<!--:--><!--:id-->Minimum saldo kepemilikan unit<!--:-->"); ?></strong></p>
										<p class="f-16"><?php the_sub_field('minimum_saldo_kepemilikan_unit');?></p>
									</div>
							</div>
						</li>
					<?php endif;?>
				<?php endwhile;?>

				<?php while (has_sub_field('custom_syarat_&_ketentuan')):?>
				<?php if(get_sub_field('content')!=""):?>
				<li class="jalur_distribusi">
					<div class="bg-white o-hidden radius-all-5">
						<div class="icon-125x100">
							<!-- <img src="<?php the_sub_field('icon_usia_masuk');?>" > -->
							<img src="<?php the_sub_field('icon');?>" class="icon-solusi">
						</div>
							<div class="details">
							<p class="uppercase f-18 m-bottom-0"><strong><?php the_sub_field('title'); ?></strong></p>
							<p class="f-16"><?php the_sub_field('content');?></p>
						</div>
					</div>
				</li>
				<?php endif;?>
				<?php endwhile;?>
			</ul>
			</div>
		</section>
	</div><!--end cover-manfaat-->
	
	<?php# endif;?>
		<?php if(get_field('matrix_brochure2')!=""):?>
		<section id="brochure-download" class="sections bg-blue clearfix">
			<ul class="pdf-list small-block-grid-2 medium-block-grid-2 large-block-grid-2 clearfix">
				<?php while (has_sub_field('matrix_brochure2')):?>
					<li><a href="<?php the_sub_field('file');?>" class="bg-white block clearfix"><span class="icon"></span><strong><?php the_sub_field('title');?></strong> <i class="fa fa-download right"></i></a></li>
				<?php endwhile;?>
			</ul>
		</section>
		<?php endif;?>
	<section id="kontak" class="sections white clearfix">
		<div id="floating" class="absolute top-0 right-55">
			<?php $title=get_the_title();
			if($title=="AXA Mandiri Travel"):?>
			<a href="https://magi-travel.appspot.com/" class="button c-blue bg-bandingkan" target="_blank">Beli Online</a>
			<?php endif;?>
			<a href="<?php echo site_url('bandingkan-produk'); ?>" class="button c-blue bg-bandingkan">Bandingkan Produk</a>
			<?php if(get_field('matrix_brochure2')!=""):?>
				<?php# while (has_sub_field('matrix_brochure2')):?>
				<?php# if(get_sub_field('file')!=""):?>
					<a href="#" class="button blue brochure-menu">Download</a>
				<?php# endif; endwhile;?>
			<?php endif;?>
		</div>
		<h3 class="fw-normal f-17 uppercase m-bottom-0 m-top-45">Kontak</h3>
		
		<div class="large-4 columns p-left-0">
			<h4 class="c-blue">Kami akan menghubungi Anda</h4>
			<p class="f-16">Isi formulir berikut untuk mendapatkan informasi yang lebih lengkap tentang produk 
dan layanan AXA Mandiri. </p>
 		</div>
		<div class="large-6 columns contact-form">

			<!-- form -->
			<form action="" id="form" name="form" method="post" class="wpcf7-form" novalidate="novalidate">
				<?php 
					$terms = wp_get_post_terms($post->ID, 'matrix_entity');
					$entity = '';
					foreach($terms as $term){
						$entity = $term->name;
						$slug=$term->slug;
					}
				?>
				<?php global $post;  ?>

				<div style="display: none;">
				<input type="hidden" name="_wpcf7" value="4">
				<input type="hidden" name="_wpcf7_version" value="3.6">
				<input type="hidden" name="_wpcf7_locale" value="en_US">
				<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4-p1330-o1">
				<input type="hidden" name="_wpnonce" value="21955172aa">
				<input type="hidden" name="product_matrix" value="<?php echo $slug;?>">
				<input type="hidden" name="nama_produk" value="<?php the_title();?>">
				<input type="hidden" name="banner_source" value="">
				<input type="hidden" name="utm_source" value="">
				<input type="hidden" name="utm_medium" value="">
				<input type="hidden" name="utm_term" value="">
				<input type="hidden" name="utm_content" value="">
				<input type="hidden" name="utm_campaign" value="">
				<input type="hidden" name="gclid" value="">
				</div>

				<div class="fieldset">
					<span class="wpcf7-form-control-wrap nama_lengkap">
						<input type="text" name="nama_lengkap" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nama Lengkap">
					</span>
				</div>
				<div class="fieldset">
					<span class="wpcf7-form-control-wrap no_tlp">
						<input type="tel" name="no_tlp" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Nomor Telepon">
					</span>
				</div>
				<div class="fieldset">
					<span class="wpcf7-form-control-wrap email">
						<input type="email" name="email" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email">
					</span>
				</div>
				<div class="fieldset">
					<span class="wpcf7-form-control-wrap tgl_lahir">
						<input type="text" name="tgl_lahir" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required datepicker " aria-required="true" aria-invalid="false" placeholder="Tanggal Lahir" id="dp1392957616851">
					</span>
				</div>
				<div class="fieldset">
					<span class="wpcf7-form-control-wrap propinsi">
						<select name="propinsi" class="required wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
							<option value="Propinsi">Propinsi</option>
							<option value="Aceh">Aceh</option>
							<option value="Sumatera Utara">Sumatera Utara</option>
							<option value="Sumatera Barat">Sumatera Barat</option>
							<option value="Riau Ibukotanya">Riau Ibukotanya</option>
							<option value="Kepulauan Riau">Kepulauan Riau</option>
							<option value="Jambi">Jambi</option>
							<option value="Sumatera Selatan">Sumatera Selatan</option>
							<option value="Bangka Belitung">Bangka Belitung</option>
							<option value="Bengkulu">Bengkulu</option>
							<option value="Lampun">Lampung</option>
							<option value="DKI Jakarta">DKI Jakarta</option>
							<option value="Jawa Barat">Jawa Barat</option>
							<option value="Banten">Banten</option>
							<option value="Jawa Tengah">Jawa Tengah</option>
							<option value="Daerah Istimewa Yogyakarta">Daerah Istimewa Yogyakarta</option>
							<option value="Jawa Timur">Jawa Timur</option>
							<option value="Bali">Bali</option>
							<option value="Nusa">Nusa</option>
							<option value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
							<option value="Kalimantan Utara">Kalimantan Utara</option>
							<option value="Kalimantan Barat">Kalimantan Barat</option>
							<option value="Kalimantan Tengah">Kalimantan Tengah</option>
							<option value="Kalimantan Selatan">Kalimantan Selatan</option>
							<option value="Kalimantan Timur">Kalimantan Timur</option>
							<option value="Sulawesi Utara">Sulawesi Utara</option>
							<option value="Sulawesi Barat">Sulawesi Barat</option>
							<option value="Sulawesi Tengah">Sulawesi Tengah</option>
							<option value="Sulawesi Tenggara">Sulawesi Tenggara</option>
							<option value="Sulawesi Selatan">Sulawesi Selatan</option>
							<option value="Gorontalo">Gorontalo</option>
							<option value="Maluku">Maluku</option>
							<option value="Maluku Utara">Maluku Utara</option>
							<option value="Papua Barat">Papua Barat</option>
							<option value="Papua">Papua</option>
						</select>
					</span>
				</div>
				<div class="fieldset">
				 <span class="required wpcf7-form-control-wrap area">
				 	<input type="text" name="kota" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Kota / Area">
				 </span>
				</div>
				
				<div class="fieldset">
					<input type="submit" value="Kirim" class="wpcf7-form-control wpcf7-submit button small blue">
					
				</div>
				
				<div class="wpcf7-response-output wpcf7-display-none"></div>
				<p class="cap_status"></p>
				<p class="cap_sukses" style="display:none"></p>
			</form>


			<!-- form -->

		</div>
	</section>
	<?php get_template_part("widget/breadcrumbs");?>
	</div>
	<?php get_template_part("widget/hargaunit");?>
</div>
<script type="text/javascript">
			$(document).ready(function(){
		 		//captcha check
				$("#form").submit(function(){
									
				    if($("#form").valid()) {
						$.ajax({
							type: "POST",
							url: "<?=site_url()?>/axamandiri_form/kontak/submit_data?slug=<?php echo $post->post_name;?>",
							data: $('form').serialize(),
							success: function(){
								$('.cap_sukses').html("Terima kasih, data terkirim.").fadeIn(200).show();
								$('.cap_status').fadeOut(200).hide();
								document.forms["form"].reset();
							}
							});

				    }else{
						$('.cap_sukses').hide();
				    	$('.cap_status').html("Mohon periksa kembali field yang wajib diisi!").addClass('cap_status_error').fadeIn('slow');
				    	
				    }

				    return false;
				 });

		 	});
		</script>

<?php get_footer();?>