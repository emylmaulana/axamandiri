<?php/** * Template Name: Karir */?>
<?php get_header();?>

<?php
	//session_start();
	$cap = 'notEq';
	//session_start();
	$ranStr = md5(microtime());
	$ranStr = substr($ranStr, 0, 6);
	
	// $this->session->set_userdata('cap_code', $ranStr);
	$_SESSION['cap_code']=$ranStr;
	$newImage = imagecreatefromjpeg(get_bloginfo('template_url').'/images/cap_bg.jpg');
	$txtColor = imagecolorallocate($newImage, 0, 0, 0);
	imagestring($newImage, 5, 5, 5, $ranStr, $txtColor);
	//header("Content-type: image/jpeg");
	// imagejpeg($newImage, get_bloginfo('template_url').'/images/captcha.jpg');
	//imagejpeg($newImage, '/Applications/XAMPP/xamppfiles/htdocs/axaone/wp-content/themes/axaindonesia/images/captcha.jpg');
	imagejpeg($newImage, get_theme_root().'/axamandiri/images/captcha.jpg');
	$status=$_GET['succ'];
?>

<div id="page-container" style="background-image:url(<?php the_field('header_image');?>);">
	<div id="masthead" class="row relative">
		<div class="mobile-content absolute" id="header-image" style="background-image:url(<?php the_field('header_image');?>);"></div>
		<div class="content large-6">
			<h1><?php the_title(); ?></h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
		</div><!--end large 6-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->
	<div id="wrapper" class="row">
		<section class="bg-white3 clearfix">
			<div class="sections">
				<h3 class="f-24"><?php _e("<!--:en-->Why Join the AXA Mandiri?<!--:--><!--:id-->Mengapa Bergabung dengan AXA Mandiri?<!--:-->"); ?></h3>
				<?php if( have_posts() ) : the_post(); ?>
					<div class="column-2 f-16">
						 <?php the_content();?>
					 </div>
				<?php endif; wp_reset_postdata();?>
			</div>
		</section>
		<section class="bg-cover-blue clearfix bg-bluedark">
			<div class="sections p-all-60">
				<h3 class="f-24 c-white"><?php _e("<!--:en-->Career With AXA Mandiri<!--:--><!--:id-->Berkarir Bersama AXA Mandiri<!--:-->"); ?></h3>
				<p class="c-white f-16"><?php _e("<!--:en-->Dynamic workplace to help optimize your potential in developing a career<!--:--><!--:id-->Tempat kerja yang dinamis untuk membantu mengoptimalkan potensi Anda dalam mengembangkan karir<!--:-->"); ?></p>
				<ul class="switch-color clearfix small-block-grid-1 medium-block-grid-2 large-block-grid-4">
					<?php $count = 1; while(has_sub_field('karir_group')): ?>
						<li class="text-center h-220  <?=$count%2 == 0? 'bg-blue-3' : ''?>">
							<div class="p-all-15 block">
								<img src="<?php the_sub_field('image');?>"/>
								<strong class="block c-white m-top-15 f-14"><?php the_sub_field('body');?></strong>
							</div>
						</li>
					<?php $count++; endwhile;?>
				</ul>
			</div>
		</section>
		<section class="bg-grey-tipe clearfix">
			<div class="sections p-all-60 pilihan-karir">
				<h3 class="f-24 c-blue"><?php _e("<!--:en-->Career choices<!--:--><!--:id-->Pilihan Karir<!--:-->"); ?></h3>
				<p class="f-16"><?php _e("<!--:id-->AXA Mandiri berkomitmen untuk menciptakan tempat kerja yang baik di mana orang-orang dapat mengembangkan karir dan lingkungan kerja yang dinamis dapat berkembang dengan cepat. Cari posisi karir yang sesuai dengan minat dan keterampilan Anda.<!--:-->"); ?></p>
				<ul class="pilih">
					<li class="tipe-karir"><a href="http://www.jobstreet.co.id/jobs/2013/9/new/p/40/990144.htm" target='_blank'><span class="button blue ">Financial Advisor</span></li>
					<li class="tipe-karir"><a href="http://www.jobstreet.co.id/career/axaservices.htm" target='_blank'><span class="button blue ">Corporate</span></a></li>
				</ul>
			</div>
		</section>
		<!-- <section class="bg-cover-gray clearfix">
			<div class="sections p-all-60 pilihan-karir">
				<?php if(get_field('karir_amfs')): ?>
				<ul class="karir-listing clearfix small-block-grid-1 medium-block-grid-2 large-block-grid-4">
					<?php while(has_sub_field('karir_amfs')): ?>
						<li class="semua">
							<a href="http://www.jobstreet.co.id/jobs/2013/9/new/p/40/990144.htm" target="_blank" class="block bg-white clearfix h-85 o-hidden">
								<div class="bg-manfaat-right icon-90x86">
									<span class="icon" style="background: url(<?php the_sub_field('icon');?>)right no-repeat;"></span>
								</div>
								<p class="text uppercase f-13 p-all-20"><?php the_sub_field('title');?></p>
							</a>
						</li>
					<?php endwhile;?>
				</ul>
				<?php endif;?>
				<?php wp_reset_postdata();?>
			</div>
		</section> -->
		<section id="kontak" class="sections grey clearfix o-hidden">
			<div class="transparent-separate absolute" style="left:-25px;top:0;"><img src="<?php bloginfo('template_url'); ?>/images/separate-transparent.png"/></div>
			<!-- <h3 class="fw-normal f-17 uppercase m-bottom-0 m-top-45">Kontak</h3>
 -->			
			<div class="large-4 columns p-left-0">
				<h4 class="c-blue">Kirim CV Anda </h4>
				<p class="f-16">kunjungi halaman AXA Mandiri E-Recruitment untuk mengetahui informasi terkini tentang lowongan pekerjaan di AXA Mandiri, atau langsung upload CV Anda.</p>
			</div>
			<div class="large-7 columns contact-form">
				
				<!-- form -->
				<form action="<?=site_url()?>/axamandiri_form/karir/submit_data" name="form" method="post" id="form" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
					<div style="display: none;">
						<input type="hidden" name="tipe_karir" id="tipe_karir" value="">
						<input type="hidden" name="_wpcf7" value="2058">
						<input type="hidden" name="_wpcf7_version" value="3.6">
						<input type="hidden" name="_wpcf7_locale" value="id_ID">
						<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f2058-p3263-o1">
						<input type="hidden" name="_wpnonce" value="dee286a3e7">
						<!-- <input type="hidden" name="type" value="Corporate"/> -->
						<input type="hidden" name="banner_source" value="">
						<input type="hidden" name="utm_source" value="">
						<input type="hidden" name="utm_medium" value="">
						<input type="hidden" name="utm_term" value="">
						<input type="hidden" name="utm_content" value="">
						<input type="hidden" name="utm_campaign" value="">
						<input type="hidden" name="gclid" value="">

					</div>
					<ul id="selectEntity" class="small-block-grid-1 medium-block-grid-2 large-block-grid-4">
						<div class="fieldset">					
							<li class="karir-radio"><span class="checked yellow "><input type="radio" name="entity"  value="financial-advisor"></span><label for="Financial Advisor">Financial Advisor</label></li>
						</div>
						<div class="fieldset">					
							<li class="karir-radio"><span class="yellow"><input type="radio" name="entity" value="corporate"></span><label for="Corporate">Corporate</label></li>
						</div>
					</ul>
				
					<div class="fieldset">
						<span class="wpcf7-form-control-wrap nama">
							<input type="text" name="nama_lengkap" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nama Lengkap">
						</span>
					</div>
					<div class="fieldset">
						<span class="wpcf7-form-control-wrap notelp">
							<input type="text" name="no_tlp" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Nomor Telepon">
						</span>
					</div>
					<div class="fieldset">
						<span class="wpcf7-form-control-wrap email">
							<input type="email" name="email" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email">
						</span>
					</div>
					<div class="fieldset">
						<span class="wpcf7-form-control-wrap tanggallahir">
							<input type="text" name="tgl_lahir" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required datepicker " aria-required="true" aria-invalid="false" placeholder="Tanggal Lahir" id="dp1392899738518">
						</span>
					</div>
					<!-- <div class="fieldset">
						<span class="wpcf7-form-control-wrap referral">
							<input type="text" name="no_referral" value="" size="40" class=" wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="No. Referral">
						</span>
					</div> -->
					<div class="fieldset">
						<span class="wpcf7-form-control-wrap upload-cv">
							<!-- <div class="NFI-wrapper wpcf7-form-control wpcf7-file" id="NFI-wrapper-13928997386852315" style="overflow: auto; display: inline-block;">
								<div class="NFI-button NFI13928997386852315 button right" style="overflow: hidden; position: relative; display: block; float: left; white-space: nowrap; text-align: center;">
									Browse<input type="file" name="file_name" value="" size="40" class="wpcf7-form-control wpcf7-file NFI-current" aria-invalid="false" data-styled="true" style="opacity: 0; position: absolute; border: none; margin: 0px; padding: 0px; top: 0px; right: 0px; cursor: pointer; height: 60px;">
								</div>
							<input type="text"  value="" readonly="readonly" class="required NFI-filename NFI13928997386852315" name="file_name" placeholder="Upload CV" style="display: block; float: left; margin: 0px; padding: 0px 5px; width: 182px;">
						</div> -->
						<input type="file" id="file" name="file_name" class="" accept="pdf/*|msword/*|MIME_type" >
						</span>
					</div>
					<div class="fieldset">
						<!-- <span class="wpcf7-form-control-wrap captcha-karir">
							<input type="text" name="captcha-karir" value="" size="40" id="captcha" class="wpcf7-form-control wpcf7-captchar" aria-invalid="false">
							<img id="img-captcha" src="<?php bloginfo('template_directory'); ?>/images/captcha.jpg?v=<?=rand(111,999)?>" class="left m-left-5 m-right-5"/>
						</span>
 -->
						<div class="button-center">
							<input type="submit" value="submit" class="wpcf7-form-control wpcf7-submit button blue kirim-karir">
						</div>
					</div>
					
					
						
					<div class="wpcf7-response-output wpcf7-display-none"></div>
				<p class="cap_status"></p>
				<p class="cap_sukses" style="display:none"></p>
				</form>
				<!-- form -->

				
			</div>
		</section>
		<?php get_template_part("widget/breadcrumbs");?>
	</div>
</div>

<script type="text/javascript">
		$(document).ready(function(){
				function readURL(input) { 
					if (input.files && input.files[0]) { 
						// console.log(input.files);
						// console.log(input.files[0]['type']);
						if(input.files[0]['type']=='application/pdf'){
							$('.cap_status').hide();
						}else{
				    		$('.cap_status').html("Format file harus pdf atau ms.word! ").addClass('cap_status_error').fadeIn('slow');
						}
						 
					} 
				}
				$("#file").change(function () { 
					readURL(this); 
				});

				$("#form").submit(function(){ //TODO: fungsi validasi 
					var formData = new FormData($(this)[0]);
					console.log(formData);
					if($("#form").valid()) {
						// $.ajax({
						// 	type: "POST",
						// 	url: "<?=site_url()?>/axamandiri_form/karir/submit_data", //TODO: submit to karir controller, submit_data function.
						// 	data: formData,
						// 	success: function(){
						// 		$('.cap_sukses').html("Terima kasih, CV terkirim.").fadeIn(200).show();
						// 		$('.cap_status').fadeOut(200).hide();
						// 		document.forms["form"].reset();
						// 	}
						
						// });
						return true;
				    }else{
						$('.cap_sukses').hide();
				    	$('.cap_status').html("Mohon periksa kembali field yang wajib diisi!").addClass('cap_status_error').fadeIn('slow');
				    	
				    }


				    return false;
				 });

			$('input[name=entity]').change(function(){
      			var tax = new Array();
      			// console.log(tax);
      			$('input[name=entity]:checked').each(function(){
      				tax.push(this.value);
      			});
      			tax = tax.join(",");
      			console.log(tax);
      			$('#tipe_karir').val(tax);
      		});	

      		var thisURLSuccess = '<?php echo site_url();?>/karir/?succ=Data%20Tersimpan';
				if (window.location.href == thisURLSuccess ) {
					$('.cap_sukses').html('<?php echo $status?>').show();
				}
		 });
					
		
// just for the demos, avoids form submit


		

			
		 	

		</script>

<?php get_footer();?>