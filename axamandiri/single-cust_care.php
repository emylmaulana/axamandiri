<?php include "wp-blog-header.php"; ?>
    
    <?php if( have_posts() ) : the_post(); ?>

 <!--    <div id="tableArray" style="display:none;"><?php the_content(); ?></div> -->
    <ul id="contact-axa">
        <li class="phone"><i class="fa fa-phone-square"></i> <?php the_field('unit_phone_number');?></li>
        <?php if(get_field('unit_sms')):?><li class="sms c-blue"><i class="fa fa-comments" style="text-align:center;font-size:20px;border-radius: 4px;"></i> <span class="f-12" style="margin-left:5px;"><?php the_field('unit_sms');?></span></li><?php endif;?>
        <?php if(get_field('unit_voice')):?><li class="sms c-blue"><i class="fa fa-mobile" style="text-align:center;font-size:20px;color: #fff;background: #053584;border-radius: 4px;"></i> <span class="f-12" style="margin-left:5px;"><?php the_field('unit_voice');?></span></li><?php endif;?>
        <li class="mail"><i class="fa fa-envelope"></i> <a href="mailto:<?php the_field('unit_email');?>" target="_blank">Email Kami</a></li>
    </ul>

    <!-- <div id="visualization"></div>< visualization --> 

    <?php endif;?>
