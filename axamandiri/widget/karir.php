<div id="karir-widget" class="clearfix box h-255">
	<div class="two_grid">
		<div class="img-content">
			<img src="<?php bloginfo('template_url');?>/images/direktori.png"/>
		</div>

		<div class="content-text clearfix">
			<h4><?php _e("<!--:en-->Career with AXA Mandiri<!--:--><!--:id-->BERKARIR BERSAMA AXA Mandiri<!--:-->"); ?></h4>
			<p>Temukan informasi lengkap untuk bergabung dan memiliki kesempatan berkarir menjadi karyawan atau agen AXA Mandiri di seluruh Indonesia.</p>
		</div>

		<a href="<?php echo site_url('karir'); ?>" class="button right"><?php _e("<!--:en-->Career<!--:--><!--:id-->Karir <!--:-->"); ?></a>
	</div>
</div>