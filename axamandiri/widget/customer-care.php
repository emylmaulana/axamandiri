<div id="customerCare" class="cstCare widget">
	<a class="costumer block" href="#">
		<h3>Online Solution</h3>
		<img src="<?php bloginfo('template_url');?>/images/icon-yellow-cstCare.png" class="icon-top absolute"/>
		<img src="<?php bloginfo('template_url');?>/images/icon-yellow-cstCare-up.png" class="icon-bottom absolute"/>
	</a>
	<div id="details" class="clearfix p-all-20">
		<p class="title">Selamat Datang di AXA Mandiri Online Solution</p>
		<p class="m-bottom-5">Pilih layanan yang Anda butuhkan</p>
		<div class="clearfix"></div>
		<ul class="list_pembelianonline m-all-0 list-blue text-left f-14">
			<li><a href="https://magi-travel.appspot.com/ " class="block">AXA Mandiri Travel</a></li>
			<li><a href="https://axa-mandiri.force.com/corporatesolutions/LoginScreen" class="block"> AXA Mandiri e-Access</a></li>
			<li><a href="http://www.belionline.axa-mandiri.co.id" class="block">AXA Mandiri e-Commerce</a></li>
		</ul>
		

		<div class="footer-widget-custcare">
			<!-- <p>Atau</p>
			<ul class="list_pembelianonline m-all-0 list-blue text-left f-14">
				<li class="list-blue"><a href=" https://axa-mandiri.force.com/corporatesolutions/LoginScreen" class="block">AXA Mandiri E-Access</a></li>
			</ul> -->
		</div>
	</div>
</div>