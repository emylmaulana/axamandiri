<div id="mobile-service" class="bg-white clearfix">
	<div class="content-widget akses">
		<div class="img_archive left m-bottom-10">
			<img src="<?php echo bloginfo('template_url');?>/images/derek.png" />
		</div>
		<h5 class="f-16 c-blue"><?php _e("<!--:en-->DEREK AND EMERGENCY ASSISTANCE SERVICES IN HIGHWAY<!--:--><!--:id-->LAYANAN DEREK DAN BANTUAN DARURAT DI JALAN RAYA<!--:-->"); ?></h5>
		<div class="content-arcive clearfix">
			<p><?php _e("<!--:en-->We give you peace of mind when experiencing interference with a motor vehicle when driving with our service features such as Emergency Services Highways and Derek.<!--:--><!--:id-->Kami memberikan ketenangan dan kenyamanan bagi Anda saat berkendara bila mengalami gangguan pada kendaraan bermotor dengan layanan Bantuan Darurat Jalan Raya dan Layanan Derek.<!--:-->"); ?></p>
			<a href="<?php echo site_url('bantuan-jalan-raya/bantuan-darurat-jalan-raya');?>" class="left m-bottom-5 f-14"><span class="c-blue"><i class="fa fa-chevron-circle-right"></i><?php _e("<!--:en-->Aid Highway <!--:--><!--:id-->Bantuan di Jalan Raya<!--:-->"); ?></i></a>
			<a href="<?php echo site_url('bantuan-jalan-raya/layanan-derek');?>" class="left m-bottom-5 f-14"><span class="c-blue"><i class="fa fa-chevron-circle-right"></i><?php _e("<!--:en-->Layanan Derek<!--:--><!--:id-->Layanan Derek<!--:-->"); ?></i></a>
		</div>
	</div>
</div>