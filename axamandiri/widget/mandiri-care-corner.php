<div id="mandiricare" class="bg-separator clearfix">
	<div class="content-widget akses h-300">
		<div class="img_archive left m-bottom-145">
			<img src="<?php echo bloginfo('template_url');?>/images/corner.png" />
		</div>

		<div class="content-arcive clearfix">
			<h5 class="f-16 c-blue"><?php _e("<!--:en-->AXA Mandiri SELF CARE CORNER<!--:--><!--:id-->AXA MANDIRI CARE CORNER<!--:-->"); ?></h5>
			<p><?php _e("<!--:en-->Services Axa Mandiri Care Corner we present to meet the needs of the ease and convenience of access to health services.<!--:--><!--:id-->Layanan ini kami hadirkan untuk memenuhi kebutuhan akan kemudahan dan kenyamanan akses pelayanan kesehatan.<!--:-->"); ?></p>			
			<div class="button-center">
				<a href="<?php echo site_url('layanan-nasabah/customer-quick-center/care-corner');?>" class="button blue small">Lihat Detil</a>
			</div>
		</div>
	</div>
</div>

