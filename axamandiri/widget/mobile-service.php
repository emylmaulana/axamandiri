<div id="mobile-service" class="bg-separator clearfix">
	<div class="content-widget akses h-300">
		<div class="img_archive left m-bottom-145">
			<img src="<?php echo bloginfo('template_url');?>/images/mobile-service.png" />
		</div>

		<div class="content-arcive clearfix">
			<h5 class="f-16 c-blue"><?php _e("<!--:en-->AXA Mandiri MOBILE SERVICE<!--:--><!--:id-->AXA Mandiri MOBILE SERVICE<!--:-->"); ?></h5>
			<p><?php _e("<!--:en-->Take advantage of the services of AXA Mandiri Mobile Services for purposes about your policy, withdrawal of investment funds and filing claims<!--:--><!--:id-->Manfaatkan layanan AXA Mandiri MOBILE SERVICE untuk keperluan seputar polis Anda, penarikan dana investasi dan pengajuan klaim.<!--:-->"); ?></p>
			<div class="button-center">
				<a href="<?php echo site_url('layanan-nasabah/customer-quick-center/mobile-service');?>" class="button blue small">Lihat Detil</a>
			</div>
		</div>
	</div>
</div>

