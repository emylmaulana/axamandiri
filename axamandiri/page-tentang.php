<?php/** * Template Name: Tentang AXA Mandiri */?>
<?php get_header();?>
<div id="page-container" style="background-image:url(<?php the_field('header_image');?>);">
	<div id="masthead" class="row relative">
		<div class="content large-4">
			<h1><?php the_title(); ?></h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
		</div><!--end large 4-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">
		<section id="content-details" class="clearfix">
			<?php if( have_posts() ) : the_post(); ?>
				<?php the_content();?>
			<?php endif;?>

			<ul id="icons" class="clearfix">
				<li class="karyawan"></li>
				<li class="pemasaran"></li>
				<li class="nasabah"></li>
			</ul>
		</section>

		<section id="nilai-axa" class="blue-sections clearfix" style="background-image:url(<?php the_field('blue_sections_images');?>);">
			<span class="blue-sections-cover-left"></span>
			<!-- <span class="blue-sections-cover"></span> -->
			<?php if(get_field('nilai_axa')): ?>
				<?php while(has_sub_field('nilai_axa')): ?>
					<h3><?php _e("<!--:en-->POINTS OF AXA Mandiri<!--:--><!--:id-->NILAI-NILAI AXA Mandiri<!--:-->"); ?></h3>
					<div class="large-6 column first-child">
						<h4><?php the_sub_field('title');?></h4>
						<?php the_sub_field('content');?>
					</div>
					<!-- <ul id="icons" class="relative large-6 column">
						<li class="profesionalism">
							<span class="tooltip absolute">
							<strong>Professionalism</strong>
								Kami berkomitmen untuk memberikan pelayanan yang optimal dan berstandar tinggi.
							</span>
						</li>

						<li class="integrity">
							<span class="tooltip absolute">
							<strong>Integrity</strong>
								Kami mengambil langkah nyata untuk memenuhi kebutuhan nasabah dan karyawan secara efisien, akurat, dan terpercaya.
							</span>
						</li>

						<li class="pragmatism">
							<span class="tooltip absolute profesionalism">
							<strong>Pragmatism</strong>
								Kami mengimplementasikan ide ke dalam langkah nyata,<br/>serta mengomunikasikannya secara jelas dan terbuka.
							</span>
						</li>

						<li class="innovation">
							<span class="tooltip absolute profesionalism">
							<strong>Innovation</strong>
								Kami juga secara konsisten menciptakan inovasi baru sebagai nilai tambah bagi semua pemangku kepentingan.
							</span>
						</li>

						<li class="spirit">
							<span class="tooltip absolute profesionalism">
							<strong>Spirit</strong>
								Kami lakukan dengan menjunjung team spirit, semangat kebersamaan sebagai satu perusahaan, AXA.
							</span>
						</li>
					</ul> -->
				<?php endwhile;?>
			<?php endif;?>
			<span class="blue-sections-cover-right"></span>
		</section>

		<section id="axa-group" class="clearfix background-vector">
			
			<div id="logo3"></div>
			<p><?php _e("<!--:en-->AXA Mandiri operates with a focus on life insurance, general insurance and asset management through a variety of distribution channels under PT AXA Financial Indonesia, PT AXA Life Indonesia, PT Asuransi AXA Mandiri, and PT AXA Asset Management Indonesia.
						 <!--:--><!--:id-->AXA Mandiri terdiri dari bisnis asuransi jiwa, yaitu PT AXA Mandiri Financial Services dan bisnis asuransi umum, yaitu PT Mandiri AXA General Insurance, yang keduanya merupakan perusahaan patungan antara PT Bank Mandiri (Persero) Tbk dan AXA Group.<!--:-->"); 
				?>
			</p>
			<?php if(get_field('axa_indonesia_group')): ?>
				<ul class="group clearfix">
					<?php while(has_sub_field('axa_indonesia_group')): ?>
					<li> 
						<h4><?php the_sub_field('title');?></h4>
						<?php the_sub_field('content');?>
					</li>
					<?php endwhile;?>
				</ul>
			<?php endif;?>
		</section><!--end axa group-->

		<section id="board-director" class="clearfix">
			<h3>BOARD OF DIRECTORS</h3>
			<h4><?php _e("<!--:en-->Board of Directors of AXA Mandiri<!--:--><!--:id-->Jajaran Direksi AXA Mandiri<!--:-->"); ?></h4>
			<?php if(get_field('board_of_director')): ?>
				<ul class="bod clearfix">
					<?php while(has_sub_field('board_of_director')): ?>
					<li> 
						<img src="<?php the_sub_field('image');?>"/>
						<span class="details">
							<strong><?php the_sub_field('name');?></strong><br>
							<?php the_sub_field('details');?>
						</span>
					</li>
					<?php endwhile;?>
				</ul>
			<?php endif;?>
		</section><!--end board of director-->

		<section id="karir-section" class="clearfix">
			<h3 class="small-title"><?php _e("<!--:en-->CAREER<!--:--><!--:id-->KARIR<!--:-->"); ?></h3>
			<div class="large-6 column first-child">
			<h4><?php _e("<!--:en-->Be Part of AXA Mandiri <!--:--><!--:id-->Jadi Bagian dari AXA Mandiri <!--:-->"); ?></h4>
				<p><?php _e("<!--:en-->To be the company of choice for employees, AXA Mandiri always create a comfortable working environment and ensures that each employee obtain various facilities to improve their competence. AXA Mandiri improve employee engagement success led the company won the award for Best Employer in Indonesia 2011 from Aon Hewitt Consulting Asia Pacific.<!--:-->
							<!--:id-->Untuk menjadi perusahaan pilihan bagi karyawan, AXA Mandiri senantiasa menciptakan lingkungan kerja yang nyaman dan memastikan bahwa setiap karyawan memperoleh berbagai fasilitas untuk meningkatkan kompetensi mereka. Keberhasilan AXA Mandiri meningkatkan employee engagement membawa perusahaan meraih penghargaan sebagai Best Employer in Indonesia 2011 dari Aon Hewitt Consulting Asia Pacific.<!--:-->"
				); ?></p>
				<a href="<?php echo  site_url('karir');?>" class="button">
					<?php _e("<!--:en-->Join with AXA Mandiri<!--:--><!--:id-->Bergabung dengan AXA Mandiri<!--:-->"); ?></a>
			</div>
			<div class="large-5 column">
				<ul class="career-list">
					<li class="world"><span><h6>Miliki kesempatan memimpin dalam perusahaan bertaraf internasional</h6></span></li>
					<li class="innovation"><span><h6>Ciptakan produk-produk inovatif dan pelayanan yang lebih baik</h6></span></li>
					<li class="environments"><span><h6>Bekerja di lingkungan yang terbuka terhadap pengembangan diri Anda</h6></span></li>
					<li class="ladder"><span><h6>Miliki kesempatan bekerja dan belajar serta mengembangkan karir</h6></span></li>
				</ul>
			</div>
		</section><!--end karir-->

		<section id="kontak" class="bg-white3 sections clearfix relative o-hidden">
			<div class="transparent-separate absolute" style="left:-145px;top:0;"><img src="<?php bloginfo('template_url'); ?>/images/separate-transparent.png"/></div>
			<!-- <a href="" class="button floating">Download Company Profile</a> -->
			<!-- <h3 class="small-title"></h3> -->
			<h4><?php _e("<!--:en-->Financial Statements<!--:--><!--:id-->Laporan Keuangan<!--:-->"); ?></h4>

			<div class="large-3 columns show-for-large-only first-child p-left-0">
                <p class="f-16"><?php _e("<!--:en-->AXA Mandiri provides information on the annual financial statements and monthly performance reports, which penyusunananya done periodically.<!--:-->
							<!--:id-->AXA Mandiri menyediakan informasi laporan keuangan tahunan dan laporan kinerja bulanan, yang penyusunananya dilakukan secara periodik.<!--:-->"
				); ?></p>
	        </div>
	        <div class="large-9 columns">
	        		<div class="bg-greydark clearfix large-7 columns radius-all-5" style="margin-left:40px;">
	        			<div class="box p-all-10">
	        				<h5>Laporan Keuangan</h5>
	        				<form id="laporan-tahunan" action="<?php echo get_permalink(3326) ?>" method="GET">
								<?php 		
									$query = "SELECT YEAR(post_date) AS `year` FROM $wpdb->posts WHERE post_type = 'laporan_keuangan' GROUP BY YEAR(post_date) ORDER BY post_date DESC";
									$results = $wpdb->get_results($query);
									$currentYear = $results[0]->year;
									?>			
										
									<select id="laporan-tahun" class="required w-48p  columns" name="fyear">
										<option disabled selected value="">Pilih Tahun</option>
									<?php
									foreach($results as $result){
									$selected = ($_GET['fyear'] == $result->year) ? 'selected' : '';
									?>		
											<option value="<?php echo $result->year; ?>"><?php echo $result->year; ?></option>
									<?php
												}
									?>		
								</select>

								<select id='periode-keuangan' class='required right w-48p columns' name='fperiode'>
									<option disabled selected >Pilih Tipe periode</option>
									<option value="triwulan-i">Triwulan I</option>
									<option value="triwulan-ii">Triwulan II</option>
									<option value="triwulan-iii">Triwulan III</option>
									<option value="triwulan-iv">Triwulan IV</option>
									<option value="tahunan">Tahunan</option>
									
								</select>
								<!-- <select id="periode-keuangan" class="required w-48p right columns">
									<option disabled selected>Pilih Periode</option>

								</select> -->

								<div class="clearfix"></div>

								<?php
									$args2 = array(
										'hide_empty'               => 0,
										'hierarchical'             => 1,
										'taxonomy'                 => 'jenis-laporan'); 
								  $jenislaporan = get_categories($args2);
								  
								  $select = "<select id='laporan-keuangan' class='required w-48p columns' name='fjenis'>";
								  $select.= "<option disabled selected>Jenis Laporan Keuangan</option>";
								  
								  foreach($jenislaporan as $jenis){
								    if($jenis->count > 0){
								        $select.= "<option value='".$jenis->slug."'>".$jenis->name."</option>";
								    }
								  }
								  
								  $select.= "</select>";
								  
								  echo $select;
								?>
								<!-- <select id="jenis-keuangan" class="required w-48p columns">
									<option disabled selected>Jenis Laporan Keuangan</option>
								</select> -->
								

								<?php
									$args = array(
										'hide_empty'               => 0,
										'hierarchical'             => 1,
										'taxonomy'                 => 'laporan_entity'); 
								  $categories = get_categories($args);
								  
								  $select = "<select id='laporan-entity' class='required right w-48p columns' name='fentity'>";
								  $select.= "<option disabled selected>Pilih Tipe Asuransi</option>";
								  
								  foreach($categories as $category){
								    if($category->count > 0){
								        $select.= "<option value='".$category->slug."'>".$category->description."</option>";
								    }
								  }
								  
								  $select.= "</select>";
								  
								  echo $select;
								?>
								<!-- <small class="form-requirements">* Semua field wajib diisi.</small> -->
		        				<button type="submit" class="button right">Cari</button>
	        				</form>
	        			</div>
	        		</div>

	        		<div class="bg-greydark clearfix large-4 columns radius-all-5">
	        			<div class="box p-all-10">
	        				<h5>Laporan Tahunan</h5>
	        				<form id="kinerja-bulanan" action="<?php echo site_url('laporan-kinerja-bulanan');?>">
					        	<!-- <select id="kinerja-bulan">
									<option>Pilih Bulan </option>
									<option value="fmonth=1"> Januari </option>
									<option value="fmonth=2"> Februari </option>
									<option value="fmonth=3"> Maret </option>
									<option value="fmonth=4"> April </option>
									<option value="fmonth=5"> Mei </option>
									<option value="fmonth=6"> Juni </option>
									<option value="fmonth=7"> Juli </option>
									<option value="fmonth=8"> Agustus </option>
									<option value="fmonth=9"> September </option>
									<option value="fmonth=10"> Oktober </option>
									<option value="fmonth=11"> November </option>
									<option value="fmonth=12"> Desember </option>
								</select> -->

								<?php 		
									$query1 = "SELECT YEAR(post_date) AS `year` FROM $wpdb->posts WHERE post_type = 'kinerja_bulanan' GROUP BY YEAR(post_date) ORDER BY post_date DESC";
									$results1 = $wpdb->get_results($query1);
									$currentYear1 = $results1[0]->year;
									?>			
										
									<select id="kinerja-tahun" name="fyear">
										<option>Pilih Tahun</option>
									<?php
									foreach($results1 as $result1){
									$selected1 = ($_GET['fyear'] == $result1->year) ? 'selected' : '';
									?>		
											<option value="<?php echo $result1->year; ?>"><?php echo $result1->year; ?></option>
									<?php
												}
									?>		
									</select>

								<?php
									$args1 = array(
										'hide_empty'               => 0,
										'hierarchical'             => 1,
										'taxonomy'                 => 'laporan_entity',

									); 
								  $categories1 = get_categories($args1);
								  
								  $select1 = "<select id='laporan-entity' name='fentity'>";
								  $select1.= "<option  disabled selected>Pilih Tipe Asuransi</option>";
								  
								  foreach($categories1 as $category1){
								    if($category1->count > 0){
								        $select1.= "<option value='".$category1->slug."'>".$category1->description."</option>";
								    }
								  }
								  
								  $select1.= "</select>";
								  
								  echo $select1;
								?>
		        				<button type="submit" class="button right">Cari</button>
	        				</form>
	        			</div>
	        		</div>

	        </div>
	       <!-- <div class="large-4 columns branch">
	                <strong><?php _e("<!--:en-->Branch Office<!--:--><!--:id-->Kantor Cabang<!--:-->"); ?></strong>
            		<p><?php _e("<!--:en-->Find the nearest AXA Mandiri branch office in the your city<!--:--><!--:id-->Cari kantor cabang AXA terdekat di kota Anda<!--:-->"); ?></p>
        		<select id="kantor-cabang">
					<option>Pilih Kota</option>
				</select>
				<button type="input" class="button red">Cari</button>
	        </div>-->
		</section><!--end section contact us-->

		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page-container-->

<?php get_footer();?>