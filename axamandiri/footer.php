<footer id="footer" class="">
	<div class="row">
        <div id="link-cepat-footer" class="large-6  columns clearfix">
        	<div class="footer-menu">
        	<h5><?php _e("<!--:en-->Quick Menu<!--:--><!--:id-->Link Cepat<!--:-->"); ?></h5>
              <?php $quickmenu = array(
                        'theme_location'  => '',
                        'container'       => '',
                        'menu'            => 'Link Cepat',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'items_wrap'      => '<ul class="%2$s clearfix">%3$s</ul>',
                        'depth'           => 0,
                    );
                wp_nav_menu( $quickmenu );?>
                <?php $linkcepat2 = array(
                        'theme_location'  => '',
                        'container'       => '',
                        'menu'            => 'Link Cepat 2',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'items_wrap'      => '<ul class="%2$s clearfix">%3$s</ul>',
                        'depth'           => 0,
                    );
                wp_nav_menu( $linkcepat2 );?>
            </div>
        </div>
        <div id="customer-care-footer" class="large-3 columns">
            <div class="main-sites right m-top-15">
                <p id="footer-customer">Customer Care Centre</p>
                <p class="headerphone">+62 21 3005 8788</p>
                <p id="temukan">Temukan Kami</p>
            </div>
             <div id="social-footer" class="clearfix" style="clear:both;">
                <?php if(get_field('axa_mandiri_socmed', 'options')): ?>
                    <?php while(has_sub_field('axa_mandiri_socmed', 'options')): ?>
                        <div class="social-icons">
                            <a href="<?php the_sub_field('youtube'); ?>" class="icons youtube"><i class="fa fa-youtube-play"></i></a>
                            <a href="<?php the_sub_field('facebook'); ?>" class="icons facebook"><i class="fa fa-facebook"></i></a>
                            <a href="<?php the_sub_field('twitter'); ?>" class="icons twitter"><i class="fa fa-twitter"></i></a>
                        </div>
                    <?php endwhile;?>
                <?php endif;?>
            </div>
        </div>
	</div>
</footer>

<footer id="footer-copyright">
    <p>Disclaimer &amp; Ownership. Copyright 2014 AXA Mandiri Financial Services</p>

</footer>

    <div id="customercare-fixed" class="">
            <?php get_template_part('widget/customer-care');?>
    </div>
    <a class="exit-off-canvas"></a>
  </div>
</div><!--end offcanvas-->
   <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=656866527686942";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <script>
      // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      // })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      // ga('create', 'UA-33864921-7', 'sudahdistaging.com');
      // ga('send', 'pageview');
    </script>

        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-33864921-7']);
            _gaq.push(['_trackPageview']);
            (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; 

            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js'; 

            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>

    <?php wp_footer();?>

  </body>
</html>